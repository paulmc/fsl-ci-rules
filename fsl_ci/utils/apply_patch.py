#!/usr/bin/env python
#
# apply_patch.py - Apply a patch to a conda recipe
#
# This script is intended to be called manually to automatically
# update a FSL conda recipe repository.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import glob
import os.path as op
import os
import sys
import argparse
import textwrap as tw

from fsl_ci        import (tempdir,
                           sprun,
                           indir)
from fsl_ci.gitlab import (open_merge_request,
                           gen_branch_name,
                           get_default_branch,
                           gen_repository_url)
from fsl_ci.recipe import (get_recipe_variable,
                           update_recipe_variable)


SERVER_URL = 'https://git.fmrib.ox.ac.uk'


def apply_patch(patch_file):
    # assumes recipe dir is cwd

    with open('meta.yaml', 'rt') as f:
        metayaml = f.read()

    oldbld   = get_recipe_variable(metayaml, 'build')
    newbld   = int(oldbld) + 1
    metayaml = update_recipe_variable(metayaml, 'build', newbld)

    with open('meta.yaml', 'wt') as f:
        f.write(metayaml)

    with open(patch_file, 'rt') as f:
        sprun('patch -p1', stdin=f)

    backups = glob.glob('*.orig')
    if len(backups) > 0:
        sprun(f'rm -f {" ".join(backups)}')


def checkout_and_patch_recipe(recipe_path, patch_file, commitmsg,
                              server, token):

    recipe_url = gen_repository_url(recipe_path, server, token)
    defbranch  = get_default_branch(recipe_path, server, token)
    branch     = gen_branch_name('mnt/patch',
                                 recipe_path,
                                 server,
                                 token)

    if commitmsg is None:
        commitmsg = tw.dedent(f"""
        MNT: Update {recipe_path} (apply_patch.py)
        """).strip()

    with tempdir():
        sprun(f'git clone {recipe_url} recipe')
        with indir('recipe'):
            sprun(f'git checkout -b {branch} {defbranch}')
            apply_patch(patch_file)
            sprun( 'git add *')
            sprun(f'git commit -m "{commitmsg}"')
            sprun(f'git push origin {branch}')

    return branch


def parse_args():

    parser = argparse.ArgumentParser(op.basename(__file__))

    parser.add_argument('patch_file',  help='patch file')
    parser.add_argument('recipe_path', help='recipe gitlab path',
                        nargs='+')
    parser.add_argument('-s', '--server', help='gitlab server URL',
                        default=SERVER_URL)
    parser.add_argument('-t', '--token', help='gitlab API token')
    parser.add_argument('-m', '--message', help='Commit message')
    parser.add_argument('-r', '--mr_message', help='Merge request message')
    parser.add_argument('-i', '--mr_title', help='Merge request title')

    args = parser.parse_args()

    args.patch_file = op.abspath(args.patch_file)

    if args.token is None:
        args.token = os.environ['FSL_CI_API_TOKEN']

    return args


def main():

    args = parse_args()

    with open(args.patch_file, 'rt') as f:
        patch_contents = f.read()

    mrmsg = tw.dedent("""
    This merge request was triggered by the fsl-ci-rules
    `apply_patch.py` script. The patch that has been applied is as follows:
    ```
    """).strip()

    mrmsg += "\n" + patch_contents
    mrmsg += "\n```\n"

    if args.mr_message is not None:
        mrmsg += f'\n{args.mr_message}\n'

    for recipe in args.recipe_path:
        branch = checkout_and_patch_recipe(recipe,
                                           args.patch_file,
                                           args.message,
                                           args.server,
                                           args.token)

        # TODO edit merge commit message too
        mrurl = open_merge_request(recipe,
                                   branch,
                                   mrmsg,
                                   args.server,
                                   args.token,
                                   title=args.mr_title)['web_url']

        print(f'\n\nMerge request opened on {recipe}: {mrurl}')


if __name__ == '__main__':
    sys.exit(main())
