#!/usr/bin/env python

import                    argparse
import functools       as ft
import multiprocessing as mp
import os.path         as op
import                    os
import subprocess      as sp
import                    sys
import                    time
import textwrap        as tw

from fsl_ci                     import (gitlab,
                                        USERNAME,
                                        EMAIL,
                                        tempdir,
                                        indir,
                                        sprun,
                                        spcap,
                                        sprunq)
from fsl_ci.gitlab              import (gen_repository_url,
                                        get_projects_in_namespace,
                                        gen_branch_name,
                                        get_default_branch,
                                        open_merge_request,
                                        merge_merge_request,
                                        list_pipelines,
                                        cancel_pipeline)

from fsl_ci.conda               import (load_packages_from_recipe_repositories,
                                        sort_packages_by_dependence)
from fsl_ci.recipe              import (get_recipe_variable,
                                        update_recipe_variable)

from fsl_ci.utils.trigger_build import trigger_build, trigger_deploy


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


RECIPE_NAMESPACE = "fsl/conda"
"""Default recipe namespace, if not specified on the command.line."""


ALWAYS_EXCLUDE  = ['docs',
                   'installer',
                   'manifest',
                   'manifest-rules',
                   'fsl-ci-rules',
                   'fsl-gui-dashboard',
                   'fsl-gui-core',
                   'fsl-profumo']
"""List of repositories in the fsl/conda/ gitlab namespace to ignore. """


def confirm(prompt, auto=True):

    if auto and confirm.AUTO_CONFIRM:
        print(f'{prompt} [y/n]: y')
        return
    response = ''
    while response.lower() not in ('y', 'n'):
        response = input(f'{prompt} [y/n]: ').strip().lower()
    if response == 'n':
        print('Aborting')
        sys.exit(1)
confirm.AUTO_CONFIRM = False



def bump_recipe_build_number(recipe_dir):
    """Increments the build number in <recipe_dir>/meta.yaml. """

    filename = op.join(recipe_dir, 'meta.yaml')
    with open(filename) as f:
        metayaml = f.read()

    build    = get_recipe_variable(   metayaml, 'build')
    metayaml = update_recipe_variable(metayaml, 'build', int(build) + 1)

    with open(filename, 'wt') as f:
        f.write(metayaml)


def checkout_and_update_recipe(project, server, token, message=None):
    """Checks out <project>, creates a new branch, bumps the build number
    in meta.yaml, and pushes the branch to gitlab.

    The user is asked to confirm the change (at the command-line) - if they
    do not confirm, the script exits immediately.
    """

    url       = gen_repository_url(project, server, token)
    defbranch = get_default_branch(project,
                                   server,
                                   token)
    branch    = gen_branch_name(   'mnt/full_rebuild',
                                   project,
                                   server,
                                   token)

    if message is None:
        message = tw.dedent(f"""
        MNT: Update {project} recipe build number (automatic fsl-ci-rules)
        """).strip()

    with tempdir():
        sprunq(f'git clone {url} recipe')
        with indir('recipe'):
            sprunq(f'git config --local user.name  {USERNAME}')
            sprunq(f'git config --local user.email {EMAIL}')
            sprunq(f'git checkout -b {branch} {defbranch}')
            bump_recipe_build_number('.')
            sprunq( 'git add *')
            sprunq(f'git commit -m "{message}"')

            # get user to confirm MR before pushing
            diff = spcap(f'git diff {defbranch} {branch}', verbose=False)
            print(f'\n\nUpdating {project}:\n')
            print(f'{diff}\n\n')

            confirm('Approve?')

            # don't trigger a CI pipeline on the MR branch
            sprunq(f'git push --push-option=ci.skip origin {branch}')

    return branch


def update_recipe(args, project):
    """Bumps the build number on the conda recipe at <project>. """

    if args.dry_run:
        print(f'[dry run] {project}: Would bump {project} build number')
        return

    mrmsg = tw.dedent("""
    This merge request was triggered by execution of the
    fsl_ci.utils.full_rebuild script. It simply updates the
    recipe build number.
    """).strip()

    if args.message is not None:
        mrmsg += f'\n{args.message}\n'

    defbranch = get_default_branch(        project, args.server, args.token)
    branch    = checkout_and_update_recipe(project, args.server, args.token,
                                           args.message)

    result = open_merge_request( project, branch, mrmsg,
                                 args.server, args.token,
                                 title='Update build number (full_rebuild)')

    # Give gitlab a few seconds between
    # operations, and try a few times,
    # as gitlab often emits HTTP 422
    # Unprocessable entity errors.
    for i in range(3):
        time.sleep(5)
        try:
            result = merge_merge_request(project, result['iid'],
                                         args.server, args.token)
            break
        except Exception as e:
            if i == 2:
                raise e
    time.sleep(3)

    # We will trigger a build separately,
    # so cancel the pipeline that was
    # created from the merge if there was
    # one.
    pipes  = list_pipelines(project, args.server, args.token, ref=defbranch)
    pipeid = pipes[0]['id']
    if pipeid is not None:
        cancel_pipeline(project, pipeid, args.server, args.token)


def build_and_deploy_projects(args, projects):

    build    = ft.partial(trigger_build,
                          server=args.server,
                          token=args.token,
                          devrelease=args.devrelease,
                          verbose=args.debug,
                          **args.variable)
    deploy   = ft.partial(trigger_deploy,
                          server=args.server,
                          token=args.token,
                          devrelease=args.devrelease)

    if args.dry_run:
        print('[dry run] Would build and deploy')
        for project in projects:
            print(f'[dry run]   {project}')
        return

    pool = mp.Pool(args.jobs)

    try:
        # Build
        pipeids = pool.map(build, projects)

        for project, pipeid in zip(projects, pipeids):
            if pipeid is None:
                print(f'\nProject {project} build failed!')

        # Let user resolve issues by hand
        if any(p is None for p in pipeids):
            confirm('Resolve manually and proceed?', auto=False)
            confirm('Enter y when failed packages have been '
                    'deployed (n will abort)', auto=False)

        time.sleep(5)

        print('Projects built successfully\n')
        confirm('Deploy builds?')

        todeploy = [(proj, pipe) for proj, pipe in zip(projects, pipeids)
                    if pipe is not None]

        # Deploy
        results = pool.starmap(deploy, todeploy)

        if not all(results):
            for (project, pipe), result in zip(todeploy, results):
                if not result:
                    print(f'\nProject {project} deploy failed (pipeline {pipe})!')
            sys.exit(1)
        print('Projects deployed.')

    finally:
        pool.close()
        pool.join()


def parseArgs(argv=None):
    name   = op.basename(__file__)
    usage  = f'Usage: {name} -t <token> [options] project [project ...]'
    desc   = tw.dedent("""
    Trigger a full rebuild of all FSL conda packages.
    """).strip()

    helps = {
        'token'           : 'Gitlab API access token with read+write access',
        'server'          : f'Gitlab server (default: {SERVER_URL})',
        'namespace'       : f'Gitlab namespace (default: {RECIPE_NAMESPACE})',
        'production'      : 'Build production/stable version '
                            '(default: build development version)',
        'jobs'            : 'Number of projects to build in parallel (default: 4)',
        'include_noarch'  : 'Include noarch projects',
        'no_update_build' : 'Do not update the project recipe build numbers',
        'dry_run'         : "Don't do anything - just print out what would be done",
        'group_num'       : 'Start building from this project group (counting from 1)',
        'variable'        : 'Additional CI / CD variable to set for the build/deploy jobs',
        'message'         : 'Message to add to commits and merge requests',
        'debug'           : 'Print all HTTP requests',
        'yes'             : 'Respond yes to all prompts',
        'exclude'         : 'Exclude these projects',
        'project'         : 'Rebuild these projects (default: all projects in '
                            '--namespace). May be a text file containing a list '
                            'of newline-separated projects.',
    }

    parser = argparse.ArgumentParser(usage=usage, description=desc)
    parser.add_argument('-t', '--token', required=True,
                        help=helps['token'])
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=helps['server'])
    parser.add_argument('-n', '--namespace', default=RECIPE_NAMESPACE,
                        help=helps['namespace'])
    parser.add_argument('-p', '--production', action='store_false',
                        help=helps['production'], dest='devrelease')
    parser.add_argument('-j', '--jobs', type=int, default=4,
                        help=helps['jobs'])
    parser.add_argument('-i', '--include_noarch', action='store_false',
                         help=helps['include_noarch'], dest='binary_only')
    parser.add_argument('-b', '--no_update_build', action='store_false',
                        help=helps['no_update_build'], dest='update_build')
    parser.add_argument('-d', '--dry_run', action='store_true',
                        help=helps['dry_run'])
    parser.add_argument('-g', '--group_num', type=int, default=1,
                        help=helps['group_num'])
    parser.add_argument('-v', '--variable', nargs=2, metavar=('NAME', 'VALUE'),
                        action='append', help=helps['project'])
    parser.add_argument('-m', '--message', help=helps['message'])
    parser.add_argument('--debug', action='store_true', help=helps['debug'])
    parser.add_argument('-y', '--yes', action='store_true', help=helps['yes'])
    parser.add_argument('-e', '--exclude', action='append', help=helps['exclude'])

    parser.add_argument('project',  help=helps['project'], nargs='*')

    args = parser.parse_args(argv)

    if len(args.project) == 1 and op.exists(args.project[0]):
        print(f'Loading project list from file {args.project[0]} ... ', end='')
        with open(args.project[0], 'rt') as f:
            projects     = f.read().strip().split('\n')
            projects     = [p.strip() for p in projects]
            args.project = projects
        print(f'{len(args.project)} projects loaded')

    if len(args.project) > 0:
        args.project = [f'{args.namespace}/{p}' for p in args.project]

    if args.variable is None: args.variable = {}
    else:                     args.variable = dict(args.variable)

    if args.exclude is None:
        args.exclude = []

    args.always_exclude = ALWAYS_EXCLUDE

    args.always_exclude = [f'{args.namespace}/{p}' for p in args.always_exclude]
    args.exclude        = [f'{args.namespace}/{p}' for p in args.exclude]

    confirm.AUTO_CONFIRM = args.yes
    gitlab.VERBOSE       = args.debug

    return args


def main():
    args     = parseArgs()
    projects = args.project

    if args.devrelease:
        msg = 'Building DEVELOPMENT release packages, to be published ' \
              'to the development FSL conda channel...'
    else:
        msg = 'Building STABLE release packages, to be published ' \
              'to the public FSL conda channel...'
    print(f'\n\n{msg}\n\n')

    if len(projects) == 0:
        projects = get_projects_in_namespace(args.namespace,
                                             args.server,
                                             args.token)

    for project in list(projects):
        if project in args.always_exclude:
            print(f'Removing {project} (always excluded)')
            projects.remove(project)

    pkgs = load_packages_from_recipe_repositories(projects,
                                                  args.server,
                                                  args.token)
    pkgs = sort_packages_by_dependence(pkgs)

    for group in pkgs:
        for i, pkg in reversed(list(enumerate(group))):
            if pkg.name in args.exclude:
                print(f'Removing {pkg.name} (excluded)')
                group.pop(i)
            elif pkg.platforms == ['noarch']:
                print(f'Removing {pkg.recipe_path} (noarch)')
                group.pop(i)

    total_pkgs = sum([len(group) for group in pkgs])
    print(f'\nBuilding {total_pkgs} packages in {len(pkgs)} groups:')
    for i, group in enumerate(pkgs, start=1):
        print(f'Group {i}')
        for pkg in group:
            print('  ', pkg.recipe_path)

    for i, group in enumerate(pkgs, start=1):
        if i < (args.group_num):
            print(f'Skipping group {i} (requested starting group: {args.group_num})')
            continue
        print(f'\nBuilding group {i} / {len(pkgs)} ({len(group)} projects)...')
        for pkg in group:
            print(f'  - {pkg.recipe_path}')
        print()
        confirm(f'Build group {i}?')

        projects = [pkg.recipe_path for pkg in group]
        if args.update_build:
            for project in projects:
                update_recipe(args, project)
        build_and_deploy_projects(args, projects)

    print('Build complete')


if __name__ == '__main__':
    sys.exit(main())
