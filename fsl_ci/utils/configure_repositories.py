#!/usr/bin/env python
#
# This script can be used to automatically configure a gitlab project
# repository and its accompanying recipe repository so that they will
# use the common FSL CI rules.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os.path         as op
import textwrap        as tw
import                    argparse
import urllib.error    as urlerror

from fsl_ci          import (loadyaml,
                             dumpyaml)
from fsl_ci.platform import (get_skip_platform,
                             get_build_platform)
from fsl_ci.gitlab   import (get_project_metadata,
                             set_project_metadata,
                             get_default_branch,
                             list_project_branches,
                             download_file,
                             update_file,
                             find_suitable_runners,
                             enable_runner,
                             get_runner_metadata,
                             create_or_update_variable,
                             create_branch,
                             protect_branch,
                             open_merge_request,
                             trigger_pipeline)
from fsl_ci.conda    import (is_standard_recipe_path,
                             load_meta_yaml)


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


CUSTOM_CI_PATH = '.gitlab-ci.yml@fsl/conda/fsl-ci-rules'
"""Default custom CI configuration path to use if not specified on
command-line.
"""

RUNNER_TAGSETS = ['fslconda-channel-host',
                  'docker,fsl-ci',
                  'linux-aarch64,fsl-ci',
                  'macOS-64,fsl-ci',
                  'macOS-M1,fsl-ci']
"""Runners which match any of these tag sets are enabled on the specified
project/recipe repositories.
"""


def get_gitlab_ci_yml(project_path, server, token):
    """Returns the name of the .gitlab-ci.yml (or otherwise named) file,
    and its contents, if the project has one, None otherwise.
    """

    meta    = get_project_metadata(project_path, server, token)
    ci_path = meta['ci_config_path']

    if ci_path in ('', 'null', None):
        ci_path = '.gitlab-ci.yml'

    try:
        return ci_path, download_file(project_path, ci_path, server, token)
    except urlerror.HTTPError as e:
        if e.code == 404:
            return None
        else:
            raise e


def patch_gitlab_ci_yml(project_path, server, token, ci_path):
    """If the project repository already has a .gitlab-ci.yml file, it is
    patched to integrate the fsl-ci-rules into the existing project pipeline.
    """

    ciyml, contents      = get_gitlab_ci_yml(project_path, server, token)
    contents             = loadyaml(contents)
    rulespath, rulesrepo = ci_path.split('@')
    changes_made         = False

    branch = 'ci/integrate-fsl-ci-rules-configure-repositories'
    msg    = f'CI: Integrate common fsl/conda/fsl-ci-rules into ' \
             f'{ciyml} pipelines (auto-generated fsl-ci-rules)'
    mrmsg  = tw.dedent(f"""
    This merge request was triggered by an invocation of the
    fsl/conda/fsl-ci-rules/ci_utils/configure_repositories.py script.

    It integrates the common FSL CI rules from the fsl/conda/fsl-ci-rules
    repository into the CI pipeline of this project ({project_path}).

    Unfortunately it is difficult to preserve the formatting of a
    YAML file when programmatically modifying it, so apologies if
    the modified version has messed up your formatting.
    """).strip()

    if branch in list_project_branches(project_path, server, token):
        print(f'A MR to integrate fsl-ci-rules into {project_path} '
              'already appears to be active - skipping.')
        return

    cistages = ['fsl-ci-pre', 'fsl-ci-build', 'fsl-ci-test', 'fsl-ci-deploy']
    stages   = [s for s in contents['stages'] if s not in cistages] + cistages

    include = {
        'project' : rulesrepo,
        'file'    : rulespath
    }

    if stages != list(contents['stages']):
        changes_made = True
        contents['stages'] = stages

    if 'include' not in contents:
        contents['include'] = []
    if include not in contents['include']:
        changes_made = True
        contents['include'].append(include)

    if changes_made:
        contents  = dumpyaml(contents)
        defbranch = get_default_branch(project_path, server, token)
        create_branch(project_path, branch, defbranch, server, token)
        update_file(project_path, ciyml, contents, msg, server, token, branch)
        open_merge_request(project_path,
                           branch,
                           mrmsg,
                           server,
                           token,
                           defbranch)
    else:
        print(f'fsl-ci-rules already appears to be integrated '
              f'in the CI rules for {project_path} - skipping.')


def set_variables(project_path,
                  recipe_path,
                  server,
                  token,
                  ci_token,
                  repo=None,
                  branch=None):
    """Sets variables on the project and recipe repositories which are needed
    for it to use the fsl-ci-rules.
    """

    project_meta = {
        'shared_runners_enabled' : 'true',
        'keep_latest_artifact'   : 'false'
    }

    if project_path is not None:
        set_project_metadata(project_path, server, token, project_meta)
    if recipe_path is not None:
        set_project_metadata(recipe_path,  server, token, project_meta)

    meta  = download_file(recipe_path, 'meta.yaml', server, token)
    meta  = load_meta_yaml(meta)
    skip  = get_skip_platform(meta)
    build = get_build_platform(meta)

    if project_path is not None:
        create_or_update_variable(
            project_path, server, token, 'FSL_CI_API_TOKEN',
            ci_token, masked=True)

    if recipe_path is not None:
        create_or_update_variable(
            recipe_path, server, token, 'FSL_CI_API_TOKEN',
            ci_token, masked=True)
        create_or_update_variable(
            recipe_path, server, token, 'FSLCONDA_RECIPE', 1)
        create_or_update_variable(
            recipe_path, server, token, 'FSLCONDA_SKIP_PLATFORM', skip)
        create_or_update_variable(
            recipe_path, server, token, 'FSLCONDA_BUILD_PLATFORM', build)
        create_or_update_variable(
            recipe_path, server, token, 'FSL_CI_SKIP_TEST', 1)


    # This causes the recipe to be built from the specified
    # git repository, instead of the one specified in the
    # recipe meta.yaml. Used during development, as some
    # projects are being built from forks.
    if (repo is not None) and (recipe_path is not None):
        create_or_update_variable(
            recipe_path, server, token, 'FSLCONDA_REPOSITORY', repo)

    # This causes the recipe to be built from the specified
    # project ref, instead of the ref specified in the recipe
    # meta.yaml. Used during development (as during
    # development, all FSL projects are being built from a
    # branch on each project repo called mnt/conda).
    if (branch is not None) and (recipe_path is not None):
        create_or_update_variable(
            recipe_path, server, token, 'FSLCONDA_REVISION', branch)

    if project_path is not None:
        if not is_standard_recipe_path(project_path, recipe_path):
            create_or_update_variable(
                project_path, server, token,
                'FSLCONDA_RECIPE_URL', f'{server}/{recipe_path}')


def find_and_enable_runner(project_path, runner_tags, exclude, server, token):
    """Identifies and enables runners on the given project which have
    the given tags.
    """
    rids = find_suitable_runners(project_path, runner_tags, server, token)

    for r in rids:
        meta  = get_runner_metadata(r, server, token)
        names = [meta['name'], meta['description']]
        names = [n.lower() for n in names if n is not None]
        if any(e in exclude for e in names):
            print(f'Excluding runner {names}')
        else:
            enable_runner(project_path, r, server, token)


def trigger_initial_pipeline(project_path, server, token, ref=None):
    """Runs an intiial pipeline on the given project repository. """

    if ref is None:
        ref = get_default_branch(project_path, server, token)

    trigger_pipeline(project_path, ref, server, token)


def parseArgs(argv=None):
    name   = op.basename(__file__)
    usage  = f'Usage: {name} -t <token> [options] project_path recipe_path'
    desc   = tw.dedent("""
    This script does the following:

      - Sets environment variables required by the fsl-ci-rules on both the
        project and recipe repositories.

      - Enables shared gitlab runners, and any specific runners given on the
        command-line, on both the project and recipe repositories.

      - Sets the CI configuration path to refer to the fsl-ci-rules
        configuration on both the project and recipe repositories. Or,
        if the project has its own CI configuration, patches the
        project .gitlab-ci.yml file to integrate the fsl-ci-rules into
        it.

      - Protects the master, main, and maint/* branches of the recipe
        repository so that commits must be added via merge requests.

      - If --run_pipeline is used, triggers an initial pipeline to build a
        development package. Note that deployment of the development package
        must be manually invoked, but you can use the trigger_deploy.py script
        to do this from the command-line.
    """).strip()
    default_runner_tags = ','.join([f'"{ts}"' for ts in RUNNER_TAGSETS])
    default_runner_tags = f'[{default_runner_tags}]'
    parser = argparse.ArgumentParser(
        usage=usage,
        description=desc,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    helps  = {
        'project_path' :
        'Path (namespace/project) of the gitlab project repository. Pass '
        'an empty string to bypass project repository configuration.',

        'recipe_path' :
        'Path (namespace/project) of the gitlab recipe repository. Pass '
        'an empty string to bypass recipe repository configuration.',

        'token' :
        'Gitlab API access token with read+write access',

        'ci_token' :
        'Token to be used by project CI jobs (via the FSL_CI_API_TOKEN '
        'variable). If not provided, --token is used.',

        'ci_path' :
        f'Custom CI configuration path to use (default {CUSTOM_CI_PATH})',

        'server' :
        f'Gitlab server (default: {SERVER_URL})',

        'runner_tags' :
        'Enable all available gitlab runners which have these '
        f'(comma-separated) tags (default {default_runner_tags}). '
        'Can be used multiple times. Case insensitive match.',

        'exclude_runner' :
        'Do not enable runners with the specified name or description '
        'Can be used muitiple times. Case insensitive match.',

        'run_pipeline' :
        'Triggers an initial pipeline to build a development package.',

        'repo' :
        'Set FSLCONDA_REPOSITORY on the recipe repository to build from this '
        'project repository, instead of the one specified in the recipe '
        'meta.yaml.',

        'branch' :
        'Set FSLCONDA_REVISION on the recipe repository to build from this '
        'project branch (e.g. "mnt/conda"). Also used by --run_pipeline '
        'so that the pipeline is triggered to run off the specified branch '
        '(default: master)'
    }

    parser.add_argument('project_path',           help=helps['project_path'])
    parser.add_argument('recipe_path',            help=helps['recipe_path'])
    parser.add_argument('-r', '--runner_tags',    help=helps['runner_tags'],
                        action='append')
    parser.add_argument('-e', '--exclude_runner', help=helps['exclude_runner'],
                        action='append')
    parser.add_argument('-s', '--server',         help=helps['server'],
                        default=SERVER_URL)
    parser.add_argument('-t', '--token',          help=helps['token'],
                        required=True)
    parser.add_argument('-c', '--ci_token',       help=helps['ci_token'])
    parser.add_argument('-p', '--ci_path',        help=helps['ci_path'])
    parser.add_argument('-rp', '--run_pipeline',  help=helps['run_pipeline'],
                        action='store_true')
    parser.add_argument('-re', '--repo',          help=helps['repo'])
    parser.add_argument('-b', '--branch',         help=helps['branch'])

    args = parser.parse_args(argv)

    args.project_path = args.project_path.strip()
    args.recipe_path  = args.recipe_path .strip()

    if args.project_path == '': args.project_path = None
    if args.recipe_path  == '': args.recipe_path  = None

    if args.ci_token is None:
        args.ci_token = args.token

    args.token    = args.token   .strip()
    args.ci_token = args.ci_token.strip()

    if args.ci_path is None:
        args.ci_path = CUSTOM_CI_PATH

    if args.runner_tags is None:
        args.runner_tags = list(RUNNER_TAGSETS)

    if args.exclude_runner is None:
        args.exclude_runner = []

    args.exclude_runner = [r.lower() for r in args.exclude_runner]
    args.runner_tags    = [r.lower() for r in args.runner_tags]

    for i, rtags in enumerate(args.runner_tags):
        args.runner_tags[i] = rtags.split(',')

    return args


def main(argv=None):
    args = parseArgs(argv)
    print(f'Enabling shared runners and setting CI variables '
          f'on {args.project_path} and {args.recipe_path}...')
    set_variables(args.project_path,
                  args.recipe_path,
                  args.server,
                  args.token,
                  args.ci_token,
                  args.repo,
                  args.branch)

    if args.recipe_path is not None:
        for branch in ('master', 'main', 'maint/*'):
            print(f'Protecting the {branch} branch of '
                  f'the {args.recipe_path} repository')

    for rtags in args.runner_tags:

        if args.project_path is not None and rtags != ['fslconda-channel-host']:

            print(f'Enabling runners with tags {rtags} '
                  f'on {args.project_path}...')
            find_and_enable_runner(args.project_path,
                                   rtags,
                                   args.exclude_runner,
                                   args.server,
                                   args.token)

        if args.recipe_path is not None:
            print(f'Enabling runners with tags {rtags} '
                  f'on {args.recipe_path}...')
            find_and_enable_runner(args.recipe_path,
                                   rtags,
                                   args.exclude_runner,
                                   args.server,
                                   args.token)

    if args.project_path is not None:
        if get_gitlab_ci_yml(args.project_path,
                             args.server,
                             args.token) is not None:
            print(f'Opening a MR on {args.project_path} to patch '
                  'the CI config and integrate fsl-ci-rules...')
            patch_gitlab_ci_yml(args.project_path,
                                args.server,
                                args.token,
                                args.ci_path)
        else:
            print(f'Setting the CI config path on '
                  f'{args.project_path} to {args.ci_path}...')
            set_project_metadata(args.project_path,
                                 args.server,
                                 args.token,
                                 data={'ci_config_path' : args.ci_path})

    if args.recipe_path is not None:
        print(f'Setting the CI config path on '
              f'{args.recipe_path} to {args.ci_path}...')
        set_project_metadata(args.recipe_path,
                             args.server,
                             args.token,
                             data={'ci_config_path' : args.ci_path})

    if (args.project_path is not None) and args.run_pipeline:
        print('Running initial pipeline on '
              f'{args.project_path}:{args.branch}')
        trigger_initial_pipeline(args.project_path,
                                 args.server,
                                 args.token,
                                 args.branch)


if __name__ == '__main__':
    main()
