#!/usr/bin/env python
#
# This script is not executed as part of any CI job, but is a convenience
# script which allows environment variables to be set on gitlab repositories
# at the command-line, rather than via the gitlab web interface.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path         as op
import textwrap        as tw
import                    sys
import                    argparse

from fsl_ci.gitlab import (create_or_update_variable,
                           delete_variable)


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


def parseArgs():

    name  = op.basename(__file__)
    usage = tw.dedent(f"""
    Usage: {name} -t <token>    project [project ...]    var=val [var=val ...]
           {name} -t <token>    project_list.txt         var_list.txt
           {name} -t <token> -d project [project ...] __ var [var ...]
           {name} -t <token> -d project_list.txt         var_list.txt
    """).strip()

    desc = tw.dedent("""
    In the second form, project_list.txt is a text file with format:
      project1
      namespace1/project2
      ...

    And var_list is a text file with format:
      var1=val1
      var2=val2
      ...

    In the third and fourth forms, all specified variables are deleted. In the
    third form, the projects and variableds must be separated by a literal
    double-underscore.

    In the fourth form, var_list.txt is a text file with format:
      var1
      var2
      ...

    The -t option is required, and must be a Gitlab API access token with
    read+write privileges on all of the specified projects.""").strip()

    helps = {
        'project_or_variable_or_file' :
        'Gitlab project, variable=value pair, variable name, or text file',

        'token' :
        'Gitlab API access token with read+write access',

        'server' :
        f'Gitlab server (default: {SERVER_URL})',

        'delete' :
        'Delete specified variables',

        'mask' :
        'Mask the variables',
    }

    parser = argparse.ArgumentParser(
        usage=usage,
        description=desc,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('project_or_variable_or_file', nargs='*',
                        help=helps['project_or_variable_or_file'])
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=helps['server'])
    parser.add_argument('-t', '--token', required=True,
                        help=helps['token'])
    parser.add_argument('-d', '--delete', action='store_true',
                        help=helps['delete'])
    parser.add_argument('-m', '--mask', action='store_true',
                        help=helps['mask'])

    args    = parser.parse_args()
    posargs = args.project_or_variable_or_file

    if len(posargs) < 2:
        print(parser.format_help())
        sys.exit(1)

    if all((len(posargs) == 2,
            op.exists(posargs[0]),
            op.exists(posargs[1]))):

        projfile, varfile = posargs
        with open(projfile) as f: projects  = f.read().strip().split('\n')
        with open(varfile)  as f: variables = f.read().strip().split('\n')

    elif args.delete:
        div       = posargs.index('__')
        projects  = posargs[:div]
        variables = posargs[div + 1:]

    else:
        projects  = [a for a in posargs if '=' not in a]
        variables = [a for a in posargs if '='     in a]

    projects = [p.strip() for p in projects]

    if not args.delete:
        variables = [v.split('=')          for v    in variables]
        variables = {k.strip() : v.strip() for k, v in variables}

    args.projects  = projects
    args.variables = variables

    return args


def main():
    args = parseArgs()

    for project in args.projects:

        if args.delete:
            for key in args.variables:
                print(f'Deleting {project}: {key}')
                delete_variable(project, args.server, args.token, key)
        else:
            for key, value in args.variables.items():
                print(f'Setting {project}: {key}={value}')
                create_or_update_variable(
                    project, args.server, args.token, key, value,
                    masked=args.mask)


if __name__ == '__main__':
    main()
