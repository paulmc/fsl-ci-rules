#!/usr/bin/env python
#
# project_dependencies.py - Identify dependencies of a FSL project.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import               os
import os.path    as op
import               sys

import fsl_ci.fsl as fsl


def main():
    if ('FSLDIR' not in os.environ) or ('FSLDEVDIR' not in os.environ):
        print('FSLDIR and FSLDEVDIR must be set!')
        sys.exit(1)

    usage = 'Usage: project_dependencies.py project_dir [-v]'

    if len(sys.argv) not in (2, 3):
        sys.exit(1)

    if not op.exists(sys.argv[1]):
        print(2, usage)
        sys.exit(1)

    if len(sys.argv) == 3 and sys.argv[2] != '-v':
        sys.exit(1)

    verbose     = len(sys.argv) == 3
    project_dir = op.abspath(sys.argv[1])
    deps        = fsl.get_project_dependencies(project_dir, verbose)

    for d in deps:
        print(d)



if __name__ == '__main__':
    main()
