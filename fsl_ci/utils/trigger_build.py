#!/usr/bin/env python
#
# trigger_build.py - Trigger a package build and deployment on one or more FSL
# conda recipe repositories.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import                          sys
import os.path               as op
import functools             as ft
import textwrap              as tw
import                          argparse
import                          datetime
import multiprocessing.dummy as mp

from   fsl_ci.recipe   import  get_recipe_variable
from   fsl_ci.platform import  get_platform_ids
from   fsl_ci.conda    import  load_meta_yaml
import fsl_ci.gitlab   as      gitlab
from   fsl_ci.gitlab   import (trigger_job,
                               gen_project_path,
                               get_variables,
                               get_default_branch,
                               download_file,
                               find_latest_job,
                               trigger_pipeline,
                               wait_on_pipeline)



SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


def now():
    """Returns the current time as a string. """
    return datetime.datetime.now().strftime('%H:%M:%S')


def get_revision(recipe_path, server, token):
    """Return the value of the FSLCONDA_REVISION variable on the given
    conda recipe repository, or None if it is not set.

    This specifies the revision on the project repository that is to be
    built.

    The returned revision is only used for development builds.
    """

    meta         = download_file(recipe_path, 'meta.yaml', server, token)
    project_repo = get_recipe_variable(meta, 'repository')
    variables    = get_variables(recipe_path, server, token)

    # meta.yaml not parseable
    if project_repo is None:
        return None

    # externally hosted project
    if SERVER_URL not in project_repo:
        return None

    # Build off FSLCONDA_REVISION if set, otherwise
    # build off master (remember this only affects
    # development builds)
    rev = variables.get('FSLCONDA_REVISION', None)

    if rev is not None:
        return rev
    # Build a dev build from the latest commit
    # on the default branch. get_default_branch
    # may raise an error for e.g. external
    # projects
    project_path = gen_project_path(project_repo)
    try:
        return get_default_branch(project_path, server, token)
    except Exception:
        return None


def trigger_build(project, server, token, devrelease, verbose=True, **kwargs):
    """Triggers a pipeline on the master branch of project and waits for it
    to complete.
    """

    rev       = get_revision(project, server, token)
    defbranch = get_default_branch(project, server, token)
    variables = dict(kwargs)

    if devrelease:
        channel   = 'development'
        timestamp = datetime.datetime.utcnow().strftime('%Y%m%d%H%M')
        variables['DEVRELEASE']           = 'true'
        variables['DEVRELEASE_TIMESTAMP'] = timestamp

        if rev is not None:
            variables['FSLCONDA_REVISION'] = rev
    else:
        channel = 'public'

    try:
        pipeline = trigger_pipeline(
            project, defbranch, server, token, variables)
    except Exception:
        return None

    pid = pipeline['id']

    print(f'{now()} Pipeline triggered on {project} ({channel} build) '
          f'- see {pipeline["web_url"]}')

    try:
        status = wait_on_pipeline(project, pid, server, token, verbose=verbose)
    except Exception:
        return None

    print(f'{now()} Build pipeline for {project} has finished: {status}')

    if status != 'manual':
        return None

    return pid


def trigger_deploy(project, pid, server, token, devrelease, sep_platforms=False):
    """Triggers the most recently created manual 'deploy-conda-package' job,
    and waits for it to complete.
    """

    # trigger_build returns None
    # if the build failed
    if pid is None:
        return False

    # deployment to devel/production gets set at
    # build time, so we don't need to pass the DEVRELEASE
    # variable here, like we do in trigger_build
    if devrelease: channel = 'development'
    else:          channel = 'public'

    meta      = download_file(project, 'meta.yaml', server, token)
    meta      = load_meta_yaml(meta)

    if sep_platforms:
        platforms = get_platform_ids(meta)
        jobpats   = [f'deploy-{p}-conda-package' for p in platforms]
    else:
        jobpats   = ['deploy-all-conda-packages']

    print(f'{now()} Triggering deploy-conda-package jobs '
          f'on {project} (deploying to {channel} channel)')

    try:
        jids = []
        for jobpat in jobpats:
            jids.extend(find_latest_job(
                project, server, token, jobpat=jobpat, pid=pid))
        for j in jids:
            trigger_job(project, j['id'], server, token)

        status = wait_on_pipeline(project, pid, server, token, timeout=10)
        print(f'{now()} Deploy job {project} has finished: {status}')
        return True
    except Exception as e:
        print(f'Error triggering deploy job on {project}: {e}')
        return False


def parseArgs(argv=None):
    """Parses and returns command line arguments. """

    name   = op.basename(__file__)
    usage  = f'Usage: {name} -t <token> [options] project [project ...]'
    desc  = tw.dedent("""
    Trigger a package build and deployment on
    one or more FSL conda recipe repositories.
    """).strip()

    helps = {
        'token'      : 'Gitlab API access token with read+write access',
        'server'     :  f'Gitlab server (default: {SERVER_URL})',
        'project'    : 'Project(s) to build',
        'jobs'       : 'Number of projects to build in parallel (default: 4)',
        'production' : 'Build production/stable version '
                       '(default: build development version)',
        'debug'      : 'Print all HTTP requests',
        'variable'   : 'Additional CI / CD variable to set for the '
                       'build/deploy jobs',
    }

    parser = argparse.ArgumentParser(usage=usage, description=desc)
    parser.add_argument('project', nargs='+',
                        help=helps['project'])
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=helps['server'])
    parser.add_argument('-t', '--token', required=True,
                        help=helps['token'])
    parser.add_argument('-j', '--jobs', type=int, default=4,
                        help=helps['jobs'])
    parser.add_argument('-p', '--production', action='store_false',
                        help=helps['production'], dest='devrelease')
    parser.add_argument('-d', '--debug', action='store_true',
                        help=helps['debug'])
    parser.add_argument('-v', '--variable', nargs=2, metavar=('NAME', 'VALUE'),
                        action='append', help=helps['variable'])

    args = parser.parse_args(argv)

    if args.variable is None: args.variable = {}
    else:                     args.variable = dict(args.variable)

    gitlab.VERBOSE = args.debug

    return args


def main(argv=None):
    """Trigger builds on all listed projects concurrently, and wait for them
    all to complete or fail.
    """

    args     = parseArgs(argv)
    projects = args.project
    build    = ft.partial(trigger_build,
                          server=args.server,
                          token=args.token,
                          devrelease=args.devrelease,
                          **args.variable)
    deploy   = ft.partial(trigger_deploy,
                          server=args.server,
                          token=args.token,
                          devrelease=args.devrelease)

    pool   = mp.Pool(args.jobs)
    pids   = pool.map(build, projects)
    result = all(pool.starmap(deploy, zip(projects, pids)))

    pool.close()
    pool.join()

    return 0 if result else 1


if __name__  == '__main__':
    sys.exit(main())
