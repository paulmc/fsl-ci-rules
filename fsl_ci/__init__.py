#!/usr/bin/env python
#
# __init__.py - Miscellaneous functions used throughout fsl_ci.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path      as op
import                 os
import                 sys
import                 errno
import                 shlex
import                 time
import                 tempfile
import                 threading
import urllib.parse as urlparse
import contextlib   as ctxlib
import subprocess   as sp

try:                import yaml
except ImportError: yaml = None


__version__ = '0.59.4'
"""Current version of the fsl-ci-rules."""


USERNAME = 'fsl-ci-rules'
"""Username to be used for all git interactions which require one. """


EMAIL = 'fsl-ci-rules@git.fmrib.ox.ac.uk'
"""Password to be used for all git interactions which require one. """


def fprint(*args, **kwargs):
    """Print with flush=True. """
    print(*args, **kwargs, flush=True)


@ctxlib.contextmanager
def tempdir(changeto=True):
    """Context manager to create, and change into, a temporary directory, and
    then afterwards delete it and change back to the original working
    directory.
    """
    with tempfile.TemporaryDirectory() as td:
        prevdir = os.getcwd()

        if changeto:
            os.chdir(td)
        try:
            yield td
        finally:
            if changeto:
                os.chdir(prevdir)


@ctxlib.contextmanager
def indir(dirname):
    """Context manager to change into a directory, and then afterwards
    change back to the original working directory.
    """
    prevdir = os.getcwd()
    os.chdir(dirname)
    try:
        yield
    finally:
        os.chdir(prevdir)


def sprun(cmd, verbose=True, **kwargs):
    """Runs the given command with subprocess.run. """

    def _filterstderr(in_, out):
        def filter():
            for line in iter(in_.readline, ''):
                line = line.strip()
                line = f'\033[91m{line}\033[0m\n'
                out.write(line)

        t = threading.Thread(target=filter)
        t.daemon = True
        t.start()
        return t

    check = kwargs.pop('check', True)

    origcmd = cmd
    if not kwargs.get('shell', False):
        cmd = shlex.split(cmd)

    kwargs['text'] = True

    if verbose:
        fprint(f'Running {origcmd}')

    # default is to stream command
    # stdout/err to this stdout/err
    if 'capture_output' not in kwargs:
        if 'stdout' not in kwargs: kwargs['stdout'] = sys.stdout
        if 'stderr' not in kwargs: kwargs['stderr'] = sp.PIPE

    proc = sp.Popen(cmd, **kwargs)
    _filterstderr(proc.stderr, sys.stderr).join()
    stdout, stderr = proc.communicate()

    if check and (proc.returncode != 0):
        raise RuntimeError(f'Command returned non-0: {origcmd}')

    if check:
        return stdout, stderr
    else:
        return stdout, stderr, proc.returncode


def spcap(cmd, **kwargs):
    """Call sprun(..., capture_output=True), and return stdout as a string. """
    return sprun(cmd, stdout=sp.PIPE, **kwargs)[0]


def sprunq(cmd, **kwargs):
    """Call sprun(..., verbose=False), and return stdout as a string. """
    return spcap(cmd, verbose=False, **kwargs)


def which(command):
    """Run "which command". """
    return spcap(f'which {command}').strip()


@ctxlib.contextmanager
def lockdir(dirname):
    """Primitive mechanism by which concurrent access to a directory can be
    prevented. Attempts to create a semaphore file in the directory, but waits
    if that file already exists. Removes the file when finished.
    """

    delay    = 10
    lockfile = op.join(dirname, '.fsl_ci.lockdir')

    while True:
        try:
            fprint(f'Attempting to lock {dirname} for exclusive access.')
            fd = os.open(lockfile, os.O_CREAT | os.O_EXCL | os.O_RDWR)
            break
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise e
            fprint(f'{dirname} is already locked - '
                   f'trying again in {delay} seconds ...')
        time.sleep(10)

    fprint(f'Exclusive access acquired for {dirname} ...')
    try:
        yield

    finally:
        fprint(f'Relinquishing lock on {dirname}')
        os.close( fd)
        os.unlink(lockfile)


def loadyaml(s):
    """Loads a YAML string, returning a dict-like. """
    return yaml.load(s, Loader=yaml.BaseLoader)


def dumpyaml(o):
    """Dumps the given YAML to a string. """
    return yaml.dump(o, sort_keys=False)


def add_credentials(url, username, password):
    """Returns a URL with the username/password added."""
    username     = urlparse.quote_plus(username)
    password     = urlparse.quote_plus(password)
    scheme, path = url.split('://')
    return f'{scheme}://{username}:{password}@{path}'
