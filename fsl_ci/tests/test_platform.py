#!/usr/bin/env python
#
# test_platform.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

from unittest import mock

import pytest

import fsl_ci.platform as platform


def test_get_recipe_platform():
    tests = [
        ({},                                                             'binary'),
        ({'build': {'noarch' : 'generic'}},                              'noarch'),
        ({'build': {'noarch' : 'python'}},                               'noarch'),
        ({'build': {}},                                                  'binary'),
        ({'package' : {'name' : 'cudabad'}, 'build' : {}},               'binary'),
        ({'package' : {'name' : 'badcuda'}, 'build' : {}},               'binary'),
        ({'package' : {'name' : 'package-cuda-9.2-bad'}, 'build' : {}},  'binary'),
        ({'package' : {'name' : 'package-cuda-9.2'}, 'build' : {}},      'cuda'),
    ]

    for meta, expected in tests:
        assert platform.get_recipe_platform(meta) == expected


def test_get_platform_shorcut_if_not_applicable():

    noarch_meta = {'build' : {'noarch' : 'python'}}
    binary_meta = {}
    cuda_meta   = {'package' : {'name' : 'some-cuda-package-cuda-10.2'}}

    # (input args, expected-or-false-for-shortcut)
    tests = [
        # bad job name
        (({}, 'pkg', 'invalid-job-name', [], []), False),

        # skip platform
        (({}, 'pkg', 'build-noarch-conda-package',   ['noarch'],   ['noarch']),   False),
        (({}, 'pkg', 'build-macos-64-conda-package', ['macos-64'], ['macos-64']), False),
        (({}, 'pkg', 'build-macos-M1-conda-package', ['macos-M1'], ['macos-M1']), False),
        (({}, 'pkg', 'build-linux-64-conda-package', ['linux-64'], ['linux-64']), False),
        (({}, 'pkg', 'build-linux-aarch64-conda-package', ['linux-aarch64'], ['linux-aarch64']), False),
        (({}, 'pkg', 'build-linux-64-cuda-9.2-conda-package',
          ['linux-64-cuda-9.2'], ['linux-64-cuda-9.2']), False),

        # not in build platform
        (({}, 'pkg', 'build-noarch-conda-package',   [], []), False),
        (({}, 'pkg', 'build-macos-64-conda-package', [], []), False),
        (({}, 'pkg', 'build-macos-M1-conda-package', [], []), False),
        (({}, 'pkg', 'build-linux-64-conda-package', [], []), False),
        (({}, 'pkg', 'build-linux-aarch64-conda-package', [], []), False),
        (({}, 'pkg', 'build-linux-64-cuda-9.2-conda-package', [], []), False),

        # package doesn't match job type
        ((noarch_meta, 'pkg', 'build-linux-64-conda-package', [], ['linux-64']),
         False),
        ((noarch_meta, 'pkg', 'build-macos-64-conda-package', [], ['macos-64']),
         False),
        ((noarch_meta, 'pkg', 'build-linux-64-cuda-9.2-conda-package', [], ['linux-64-cuda-9.2']),
         False),
        ((binary_meta, 'pkg', 'build-noarch-conda-package', [], ['noarch']),
         False),
        ((binary_meta, 'pkg', 'build-linux-64-cuda-9.2-conda-package', [], ['linux-64-cuda-9.2']),
         False),
        ((cuda_meta, 'pkg', 'build-noarch-conda-package',   [], ['noarch']), False),
        ((cuda_meta, 'pkg', 'build-linux-64-conda-package', [], ['linux-64']), False),
        ((cuda_meta, 'pkg', 'build-macos-64-conda-package', [], ['macos-64']), False),

        # build the package
        ((noarch_meta, 'pkg', 'build-noarch-conda-package', [], ['noarch']),
         'noarch'),
        ((binary_meta, 'pkg', 'build-linux-64-conda-package', [], ['linux-64']),
         'linux-64'),
        ((binary_meta, 'pkg', 'build-linux-aarch64-conda-package', [], ['linux-aarch64']),
         'linux-aarch64'),
        ((binary_meta, 'pkg', 'build-macos-64-conda-package', [], ['macos-64']),
         'macos-64'),
        ((binary_meta, 'pkg', 'build-macos-M1-conda-package', [], ['macos-M1']),
         'macos-M1'),
        ((cuda_meta, 'pkg', 'build-linux-64-cuda-9.2-conda-package', [], ['linux-64-cuda-9.2']),
         'linux-64-cuda-9.2'),

        # direct deploy job. Currenly only noarch is
        # supported, although this is not restricted
        # in the code.
        ((noarch_meta, 'pkg', 'build-noarch-conda-package-direct-deploy', [], ['noarch']),
         'noarch'),
        ((binary_meta, 'pkg', 'build-noarch-conda-package-direct-deploy', [], ['noarch']),
         False),
    ]

    for args, expected in tests:

        if not bool(expected):
            with pytest.raises(SystemExit):
                platform.get_platform_shortcut_if_not_applicable(*args)
        else:
            print(args)
            result = platform.get_platform_shortcut_if_not_applicable(*args)
            assert result == expected, (result, expected)


def test_get_skip_platform():

    noarch_meta = {'build' : {'noarch' : 'python'}}
    binary_meta = {}
    cuda_meta   = {'package' : {'name' : 'some-cuda-package-cuda-9.2'}}

    # (input, expected)
    tests = [
        (noarch_meta, ('linux-64 '
                       'linux-aarch64 '
                       'macos-64 '
                       'macos-M1 '
                       'linux-64-cuda')),
        (binary_meta, ('noarch '
                       'linux-64-cuda')),
        (cuda_meta,   ('noarch '
                       'linux-64 '
                       'linux-aarch64 '
                       'macos-64 '
                       'macos-M1'))
    ]

    for input, expected in tests:
        assert platform.get_skip_platform(input) == expected


def test_get_build_platform():

    noarch_meta = {'build' : {'noarch' : 'python'}}
    binary_meta = {}
    cuda_meta   = {'package' : {'name' : 'some-cuda-package-cuda-9.2'}}

    # (input, expected)
    tests = [
        (noarch_meta, 'noarch'),
        (binary_meta, ('linux-64 '
                       'linux-aarch64 '
                       'macos-64 '
                       'macos-M1')),
        (cuda_meta,   'linux-64-cuda'),
    ]

    for input, expected in tests:
        assert platform.get_build_platform(input) == expected




def test_get_platform_ids():

    noarch_meta = {'build' : {'noarch' : 'python'}}
    binary_meta = {}
    cuda_meta   = {'package' : {'name' : 'some-cuda-package-cuda-11.0'}}

    # (input, expected)
    tests = [
        (noarch_meta, ['noarch']),
        (binary_meta, ['linux-64', 'linux-aarch64', 'macos-64', 'macos-M1']),
        (cuda_meta,   ['linux-64-cuda-9.2',
                       'linux-64-cuda-10.2',
                       'linux-64-cuda-11.0',
                       'linux-64-cuda-11.1',
                       'linux-64-cuda-11.3'])]

    with mock.patch('fsl_ci.platform.CUDA_VERSIONS',
                    ['9.2', '10.2', '11.0', '11.1', '11.3']):
        for input, expected in tests:
            assert platform.get_platform_ids(input) == expected
