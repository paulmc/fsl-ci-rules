if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper fdt_matrix_merge find_the_biggest label2surf probtrackx2 probtrackx2_gpu proj_thresh surf2surf surf2volume surf_proj surfmaths
fi
