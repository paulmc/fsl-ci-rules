#!/usr/bin/env python
#
# test_fsl.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os

import fsl_ci.fsl as fsl

from fsl_ci import tempdir

from fsl_ci.tests import create_mock_python_project


def test_get_python_entrypoints():
    with tempdir():
        os.mkdir('project')
        create_mock_python_project('project', 2)
        expect = ['entrypoint0 = pkg.entrypoint0:main',
                  'entrypoint1 = pkg.entrypoint1:main']
        assert fsl.get_python_entrypoints('project') == expect


def test_get_python_executables():
    with tempdir():
        os.mkdir('project')
        create_mock_python_project('project', 2, 2)
        expect = ['entrypoint0', 'entrypoint1', 'script0', 'script1']
        assert fsl.get_python_executables('project') == expect
