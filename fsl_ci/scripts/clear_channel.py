#!/usr/bin/env python
#
# Remove packages from a conda channel. See the
# rules/fsl-ci-management-rules.yml:clear-public-channel and
# clear-private-channel jobs.
#
# The following variables control the behaviour of this script:
#   - DRY_RUN: If "true", don't delete anything, just print out what
#              would be deleted
#   - CLEAR:   Either "all" or "old", indicating whether all packages,
#              or all but the most recent, should be deleted.
#   - PATTERN: Delete packages matching one or more glob-style patterns.
#
# The CLEAR variable must be set - if not, the job exits without doing
# anything.
#
# If CLEAR is set to "all", *all* packages (or all packages matching PATTERN,
# if it is set) are removed.
#
# If CLEAR is set to "old", all except the most recent version
# of each package (and which match PATTERN, if it is set).
#
# The PATTERN variable can be used to limit which packages are removed.  It
# may be a semi-colon- separated list of glob-style patterns which indicate
# the packages to be deleted.  For example, to remove all packages starting
# with "abc-", and all packages with "-misc" in their name:
#
#   PATTERN="abc-*;*-misc*"
#
# Each pattern is matched against the conda package file names in each of the
# channel platform directories (noarch, linux-64, osx-64, etc), and all
# matching package files (according to the CLEAR variable) are removed.
#
# The DRY_RUN variable has a default value of "true". It must be explicitly
# set to "false" in order for packages to be removed. When DRY_RUN is not
# "false", no package files are removed - instead, the package files that
# would be removed are printed to standard output.
#


import              os
import os.path   as op
import              sys
import              pathlib
import              fnmatch

from fsl_ci            import  sprun, lockdir
from fsl_ci.conda      import (read_channel_repodata,
                               load_packages)
from fsl_ci.versioning import  parse_version


def remove(path, dry_run):
    print(f'Removing: {path}')
    if not dry_run:
        os.remove(path)


def clear_packages(channeldir, clear, patterns, dry_run):

    if patterns is not None:
        patterns = patterns.split(';')

    if op.exists(op.join(channeldir, 'channeldata.json')):
        channelurl         = pathlib.Path(channeldir).as_uri()
        chandata, platdata = read_channel_repodata(channelurl)
        packages           = load_packages(chandata, platdata)
    else:
        packages = {}

    def sortkey(pkg):
        version = pkg[1]['version']
        build   = pkg[1]['build'].split('_')[-1]
        return (parse_version(version), int(build))

    def match(filename):
        if patterns is None:
            return True
        return any(fnmatch.fnmatch(filename, p) for p in patterns)

    for pkgs in packages.values():
        pkg = pkgs[-1]
        for plat in pkg.platforms:

            # get file name + package entry
            pkgfiles = [(k, v) for k, v in platdata[plat]['packages'].items()
                        if v['name'] == pkg.name]

            # If clear == old, we want to remove all but
            # the most recent version+build, but there
            # may be more than one (e.g. python packages
            # built for different versions of python).
            if clear == 'old':
                # sort so oldest is first
                pkgfiles = sorted(pkgfiles, key=sortkey)

                # get latest version/build, and
                # remove all packages with that
                # version/build
                latest = sortkey(pkgfiles[-1])
                for i, p in reversed(list(enumerate(pkgfiles))):
                    if sortkey(p) == latest:
                        pkgfiles.pop(i)

            pkgfiles = [pf[0] for pf in pkgfiles]
            pkgfiles = [op.join(channeldir, plat, pf) for pf in pkgfiles]

            for filename in pkgfiles:
                if match(op.basename(filename)):
                    remove(filename, dry_run)


def main():

    pattern   = os.environ.get('PATTERN', None)
    clear     = os.environ.get('CLEAR',   None)
    dry_run   = os.environ.get('DRY_RUN', 'true') != 'false'
    jobname   = os.environ['CI_JOB_NAME']

    if   jobname == 'clear-public-channel':
        channeldir = os.environ['FSLCONDA_PUBLIC_CHANNEL_DIRECTORY']
    elif jobname == 'clear-internal-channel':
        channeldir = os.environ['FSLCONDA_INTERNAL_CHANNEL_DIRECTORY']
    elif jobname == 'clear-development-channel':
        channeldir = os.environ['FSLCONDA_DEVELOPMENT_CHANNEL_DIRECTORY']

    if clear is None or clear not in ('all', 'old'):
        print('CLEAR must be set to "all" or "old"')
        return 1

    print(f'Clearing {clear} packages')
    print(f'  channel: {channeldir}')
    print(f'  pattern: {pattern}')
    print(f'  dry run: {dry_run}')

    with lockdir(channeldir):
        clear_packages(channeldir, clear, pattern, dry_run)
        if not dry_run:
            sprun(f'conda index {channeldir}')

    return 0


if __name__ == '__main__':
    sys.exit(main())
