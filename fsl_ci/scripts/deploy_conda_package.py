#!/usr/bin/env python
#
# Deploy one or more built conda packages to a FSL conda channel.
#
# Deploying a built conda package is simply a matter of copying the
# built packages into the channel directory and running "conda index".
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import               glob
import itertools  as it
import os.path    as op
import               os
import               shutil
import               sys

from fsl_ci import fprint, sprun, lockdir


def reindex_channel(channeldir):
    """Run "conda index" on the conda channel directory. """
    with lockdir(channeldir):
        sprun(f'conda index {channeldir}')


def copy_packages(builddir, channeldir, overwrite):
    """copy all package files from the conda-build output directory into
    the conda channel directory. Returns the number of package files
    found and copied.
    """

    # The build_conda_package.py script stores its
    # outputs in a platform-specific subdirectory -
    # one of _linux-64_, _linux-aarch64_, _macos-64_,
    # _macos-M1_, _noarch_, _linux-64-cuda-9.2_,
    # etc. Within each one of those is a conventional
    # conda channel structure.
    fslplats   = glob.glob(op.join(builddir, '_*_'))
    condaplats = ('noarch',
                  'linux-64', 'linux-aarch64',
                  'osx-64', 'osx-arm64',
                  'win-32', 'win64')

    # build a list of (srcfile, destfile) pairs
    todeploy = []
    for fslplat, condaplat in it.product(fslplats, condaplats):
        # copy any package files that we find in
        # the build dir into the channel directory.
        srcdir  = op.join(builddir, fslplat, condaplat)
        destdir = op.join(channeldir, condaplat)
        pkgs    = glob.glob(op.join(srcdir, '*.tar.bz2')) + \
                  glob.glob(op.join(srcdir, '*.conda'))

        for srcfile in pkgs:
            destfile = op.join(destdir, op.basename(srcfile))
            if (not overwrite) and op.exists(destfile):
                raise RuntimeError(f'Package file {destfile} '
                                    'already exists - aborting!')
            todeploy.append((srcfile, destfile))

    # copy srcfiles to destfiles
    for srcfile, destfile in todeploy:
        fprint(f'Installing {destfile}...')
        if op.exists(destfile):
            fprint(f'OVERWRITING existing package file {destfile}')

        os.makedirs(op.dirname(destfile), exist_ok=True)
        shutil.copy(srcfile, destfile)

    return len(todeploy)


def main():

    recipename = os.environ['CI_PROJECT_NAME']
    recipeurl  = os.environ['CI_PROJECT_URL']
    projrepo   = os.environ.get('FSLCONDA_REPOSITORY', '')

    projrev    = os.environ.get('FSLCONDA_REVISION',   '')
    devrelease = os.environ.get('DEVRELEASE', '').lower()  == 'true'
    overwrite  = (os.environ.get('OVERWRITE',  '').lower()  == 'true') or devrelease
    internal   = 'FSLCONDA_INTERNAL' in os.environ
    builddir   = op.join(os.getcwd(), 'conda_build')

    if internal:
        channeldir = os.environ['FSLCONDA_INTERNAL_CHANNEL_DIRECTORY']
    elif devrelease:
        channeldir = os.environ['FSLCONDA_DEVELOPMENT_CHANNEL_DIRECTORY']
    else:
        channeldir = os.environ['FSLCONDA_PUBLIC_CHANNEL_DIRECTORY']

    fprint( '************************************')
    fprint(f'Deploying built conda packages for:     {recipename}')
    fprint(f'Recipe URL:                             {recipeurl}')
    fprint( 'Project repository (empty means build ')
    fprint(f'  is from repo specified in meta.yaml): {projrepo}')
    fprint( 'Project revision (empty means build is ')
    fprint(f'  from ref specified in meta.yaml):     {projrev}')
    fprint(f'FSL conda channel:                      {channeldir}')
    fprint( '************************************')

    npkgs = copy_packages(builddir, channeldir, overwrite)

    if npkgs > 0:
        reindex_channel(channeldir)
    else:
        print('No built conda packages found - aborting.')
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
