#!/usr/bin/env python
#
# Delete and re-create the index for a conda channel.
#
# I have seen instances where the "conda index" command doesn't seem to
# reliably refresh information about newly added packages, so this script
# can be manually run to purge and re-generate the index.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os
import sys

from fsl_ci import sprun, lockdir


def main():

    internal = os.environ['FSLCONDA_INTERNAL_CHANNEL_DIRECTORY']
    devel    = os.environ['FSLCONDA_DEVELOPMENT_CHANNEL_DIRECTORY']
    public   = os.environ['FSLCONDA_PUBLIC_CHANNEL_DIRECTORY']
    jobname  = os.environ['CI_JOB_NAME']

    if   'internal' in jobname: channels = [internal]
    elif 'public'   in jobname: channels = [public]
    elif 'devel'    in jobname: channels = [devel]
    else:                       channels = [internal, public, devel]

    for channeldir in channels:
        with lockdir(channeldir):
            sprun(f'find {channeldir} -name "*.html"     -delete')
            sprun(f'find {channeldir} -name "*.json"     -delete')
            sprun(f'find {channeldir} -name "*.json.bz2" -delete')
            sprun(f'conda index {channeldir}')
    return 0


if __name__ == '__main__':
    sys.exit(main())
