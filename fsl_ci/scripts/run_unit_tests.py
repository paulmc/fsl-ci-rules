#!/usr/bin/env python
#
# run_unit_tests.py - Runs any pyfeeds tests which are present in the project
# repository.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path as op
import            os
import            re
import            sys
import            glob

from fsl_ci             import (fprint,
                                sprun,
                                add_credentials)
from fsl_ci.gitlab      import (gen_project_path,)
from fsl_ci.platform    import  get_platform_shortcut_if_not_applicable
from fsl_ci.fsl_release import  (download_fsl_release,
                                 get_build_package_versions)
from fsl_ci.conda       import (condabin,
                                load_meta_yaml,
                                get_project_repository_and_revision)
import fsl_ci.utils.git_lfs_clone as git_lfs_clone


def checkout_project_repository_shortcut_if_no_tests(project_dir):
    """Checks out the project source repository - this is done to get a copy
    of the project unit tests/data.
    """
    # The FSLCONDA_REPOSITORY/REVISION vars are honoured if set
    repo, rev = get_project_repository_and_revision('meta.yaml')[:2]

    if (repo is None) or (rev is None):
        print('No repository or revision listed in meta.yaml - aborting test.')
        sys.exit(0)

    project_path = gen_project_path(repo)

    git_lfs_clone.main((project_path, project_dir, rev))

    # If no feedsRun tests can be found, we abort the job
    hits = glob.glob(op.join(project_dir, '**', 'feedsRun*'), recursive=True)

    if len(hits) == 0:
        print(f'No feedsRun scripts found in project source repository '
              f'({repo}:{rev} - aborting test.')
        sys.exit(0)

    # make sure all feedsRuns are executable
    for hit in hits:
        os.chmod(hit, 0o755)


def create_test_env(env_dir, build_dir, base_pkgs):
    """Create a conda environment to run the tests, and install pyfeeds and
    the package being tested into it.
    """

    pkgname = load_meta_yaml('meta.yaml')['package']['name']

    # The FSLCONDA_INTENRAL variable is set
    # for internal/private FSL projects, and
    # determines whether we can install
    # dependencies from the internal channel
    internal   = 'FSLCONDA_INTERNAL' in os.environ
    devrelease = os.environ.get('DEVRELEASE', '').lower() == 'true'
    intchannel = os.environ[    'FSLCONDA_INTERNAL_CHANNEL_URL']
    pubchannel = os.environ[    'FSLCONDA_PUBLIC_CHANNEL_URL']
    devchannel = os.environ[    'FSLCONDA_DEVELOPMENT_CHANNEL_URL']
    pkgs       = os.environ.get('FSL_CI_TEST_REQUIRES',               '')
    username   = os.environ.get('FSLCONDA_INTERNAL_CHANNEL_USERNAME', None)
    password   = os.environ.get('FSLCONDA_INTERNAL_CHANNEL_PASSWORD', None)

    if username is not None:
        intchannel = add_credentials(intchannel, username, password)

    if   internal:   pkgchannels = [intchannel, pubchannel]
    elif devrelease: pkgchannels = [devchannel, pubchannel]
    else:            pkgchannels = [pubchannel]

    pkgchannels = [f'-c {c}' for c in pkgchannels]
    pkgchannels = ' '.join(pkgchannels)

    pkgs = list(f'{pkgname} cxx-compiler make fsl-pyfeeds {pkgs}'.split())

    # Remove dupes - e.g. if building fsl-base,
    # we do not want to try to install the
    # version specified in fsl-release.yml.
    base_pkgs = dict(base_pkgs)
    for pkg in list(base_pkgs.keys()):
        pat = rf'{pkg}(?=[^\w-]|$)'
        if any(re.match(pat, p) for p in pkgs):
            base_pkgs.pop(pkg)

    # Make base package specs suitable for
    # passing to conda as cli args, i.e.
    # "package[=version[=build]]"
    base_pkgs = {pkg : ver.replace(" ", "=") if ver is not None else ''
                 for pkg, ver in base_pkgs.items()}
    base_pkgs = [f'{pkg}={ver}' for pkg, ver in base_pkgs.items()]

    pkgs = ' '.join(pkgs + base_pkgs)

    # Install dependencies of <pkgname> from <pkgchannels>
    # Install pyfeeds from <pkgchannels>
    conda = condabin()
    sprun(f'{conda} create -y -p {env_dir} '
          f'-c {build_dir} {pkgchannels} '
          f'-c conda-forge -c defaults {pkgs}')


def run_tests(project_dir, test_output_dir, env_dir):
    """Runs the project tests using pyfeeds. """

    cfg = op.join(project_dir, 'pyfeeds.cfg')
    if op.exists(cfg): cfg = f'-c {cfg}'
    else:              cfg = ''

    commands = []
    commands.append(f'. activate {env_dir}')
    commands.append(f'export FSLDIR={env_dir}')
    commands.append('. $FSLDIR/etc/fslconf/fsl-devel.sh')
    commands.append(f'pyfeeds run -v -k {cfg} '
                    f'-o {test_output_dir} '
                    f'{project_dir}')

    commands = '; '.join(commands)

    result = sprun(commands, shell=True, check=False)[2]

    for dirpath, _, filenames in os.walk(test_output_dir):
        for filename in filenames:
            if filename == 'feedsRun.log':
                fprint(f'\n*** {filename} ***')
                filepath = op.join(dirpath, filename)
                with open(filepath, 'rt') as f:
                    fprint(f.read())
                fprint()

    return result


def main():
    recipe_name     = os.environ['CI_PROJECT_NAME']
    job_name        = os.environ['CI_JOB_NAME']
    server          = os.environ['CI_SERVER_URL']
    token           = os.environ['FSL_CI_API_TOKEN']
    manifest_ref    = os.environ.get('FSL_MANIFEST_REVISION',   None)
    manifest_repo   = os.environ.get('FSL_MANIFEST_REPOSITORY', None)
    skip_platforms  = os.environ.get('FSLCONDA_SKIP_PLATFORM', '')
    skip_platforms  = skip_platforms.split()
    build_platforms = os.environ.get('FSLCONDA_BUILD_PLATFORM', '')
    build_platforms = build_platforms.split()
    meta            = load_meta_yaml('meta.yaml')

    # get the platform name, so we know
    # which build directory to install
    # the package from (and abort if
    # this job is not relevant for the
    # project type)
    platform  = get_platform_shortcut_if_not_applicable(
        meta, recipe_name, job_name, skip_platforms, build_platforms)
    build_dir = op.join(os.getcwd(), 'conda_build', f'_{platform}_')

    # Get versions of core/base dependencies
    # to install into test env, from the
    # fsl/conda/manifest:fsl-release.yml file.
    fsl_release = download_fsl_release(server, token,
                                       manifest_repo, manifest_ref)
    base_pkgs   = get_build_package_versions(fsl_release, platform)

    # We check out the project source
    # into a directory named with the
    # conda package name. Test outputs
    # are saved to pyfeeds_results
    project_dir     = op.abspath(recipe_name)
    test_output_dir = op.abspath('pyfeeds_results')
    env_dir         = op.abspath('test_env')

    checkout_project_repository_shortcut_if_no_tests(project_dir)
    create_test_env(env_dir, build_dir, base_pkgs)
    return run_tests(project_dir, test_output_dir, env_dir)


if __name__ == '__main__':
    sys.exit(main())
