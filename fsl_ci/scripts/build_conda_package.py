#!/usr/bin/env python
#
# Build a conda package from a FSL recipe repository.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
# (See rules/fsl-ci-build-rules.yml)


import                 glob
import                 os
import os.path      as op
import                 shutil
import urllib.parse as urlparse
import subprocess   as sp

from typing import Dict, List, Optional

import jinja2       as j2

from fsl_ci             import (sprun,
                                indir,
                                fprint,
                                dumpyaml,
                                add_credentials)
from fsl_ci.versioning  import  generate_devrelease_version
from fsl_ci.platform    import (get_recipe_platform,
                                get_platform_shortcut_if_not_applicable)
from fsl_ci.conda       import (condabin,
                                load_meta_yaml)
from fsl_ci.gitlab      import  get_revision_hash
from fsl_ci.recipe      import (get_recipe_variable,
                                patch_recipe_version,
                                render_template,
                                patch_meta_yaml_file)
from fsl_ci.fsl_release import (download_fsl_release,
                                get_build_package_versions)

def conda_build_command():
    """Return a conda build command - e.g. "conda build" or "mamba
    mambabuild"."""

    conda    = condabin()
    use_boa  = os.environ.get('FSLCONDA_USE_BOA', '') != ''
    have_boa = 'mambabuild' in sprun(f'{conda} -h', stdout=sp.PIPE)[0]

    # recent versions of mamba will invoke boa on
    # calls to "mamba build", and will crash if
    # boa is not installed. So we invoke either
    # "mamba mambabuild", or "conda build".
    conda = condabin(use_boa and have_boa)

    if use_boa and have_boa: buildcmd = f'{conda} mambabuild'
    else:                    buildcmd = f'{conda} build'

    return buildcmd


def build_recipe(recipe_dir   : str,
                 meta         : dict,
                 repo         : str,
                 ref          : str,
                 output_dir   : str,
                 pinned_pkgs  : Dict[str, str],
                 cfg_template : Optional[str],
                 extra_args   : Optional[str],
                 *channels):
    """Build the conda recipe in the given directory. """

    env      = dict(os.environ)
    channels = list(channels)

    envvar_args   = create_envvars_file(recipe_dir)
    buildcfg_args = create_build_config(meta, recipe_dir,
                                        pinned_pkgs, cfg_template)

    patch_meta_yaml(meta, recipe_dir, pinned_pkgs)

    if extra_args is None:
        extra_args = ''

    if repo in ('', None): env.pop('FSLCONDA_REPOSITORY', None)
    else:                  env['FSLCONDA_REPOSITORY'] = repo
    if ref  in ('', None): env.pop('FSLCONDA_REVISION', None)
    else:                  env['FSLCONDA_REVISION'] = ref

    # Having these environment variables set will
    # mess up clones of git lfs repositories.
    blacklist = ['GIT_CONFIG', 'GIT_CONFIG_NOSYSTEM', 'GIT_CONFIG_GLOBAL']
    for envvar in blacklist:
        env.pop(envvar, None)

    channels.extend(('conda-forge',))

    buildcmd = conda_build_command()
    cmd      = f'{buildcmd} {buildcfg_args} {envvar_args} {extra_args}'

    for chan in channels:
        cmd = f'{cmd} -c {chan}'

    sprun(f'{cmd} --output-folder={output_dir} {recipe_dir}',
          env=env)


def create_envvars_file(recipe_dir : str) -> str:
    """Create a YAML file which contains environment variables that should be
    set during the. This file is passed via conda build --append-file
    envvars.yaml.

    Returns a string containing command-line arguments to pass to conda build.
    """
    cfg_dir  = op.join(recipe_dir, 'config')
    cfg_file = op.join(cfg_dir, 'envvars.yaml')
    os.makedirs(cfg_dir, exist_ok=True)

    # Currently we are not exposing any environment
    # variables - in the future, add any required
    # variables to this candidates list.
    candidates = []
    envvars    = []

    # Only expose environment variables that are set,
    # otherwise conda will complain numerous times.
    for candidate in candidates:
        if candidate in os.environ:
            envvars.append(candidate)

    if len(envvars) == 0:
        return ''

    envvars = {'build' : {'script_env' : envvars}}

    with open(cfg_file, 'wt') as f:
        f.write(dumpyaml(envvars))

    fprint(f'\nEnvironment variable file [{cfg_file}]')
    fprint(f'{dumpyaml(envvars)}\n')

    return f'--append-file {cfg_file}'


def create_build_config(meta         : dict,
                        recipe_dir   : str,
                        pinned_pkgs  : Dict[str, str],
                        cfg_template : Optional[str]) -> str:
    """Create a conda_build_config.yaml file, and regenerated meta.yaml file,
    which specify versions of core FSL dependencies. This is used to ensure
    that FSL packages are built with the same versions of core dependencies as
    the versions that will be installed, without having to explicitly pin them
    in the project recipes.

    These files are passed to conda build like so:

        conda build --variant-config-files conda_build_config.yaml

    Returns a string containing command-line arguments to pass to conda build.
    """

    # We don't want anything pinned for generic/python
    # packages.  We want to be able to install such
    # packages into any environment, regardless of the
    # blas/boost/python/etc version.
    if get_recipe_platform(meta) == 'noarch':
        return ''

    # This is a compiled project - we want to pin core
    # C++ dependencies (boost, blas, etc), and possibly
    # Python+numpy.

    # Put config file in a sub-directory, in case the
    # recipe already has a conda_build_config.yaml file.
    dest_dir   = op.join(recipe_dir, 'config')
    templ_dir  = op.join(op.dirname(__file__), '..', 'templates')
    cfg_templ  = op.join(templ_dir, 'conda_build_config.yaml')
    cfg_dest   = op.join(dest_dir,  'conda_build_config.yaml')

    os.makedirs(dest_dir, exist_ok=True)

    # build config template can be overridden
    # by the FSLCONDA_BUILD_CONFIG_TEMPLATE var
    if cfg_template is not None:
        cfg_templ = op.join(dest_dir, 'conda_build_config.yaml.template')
        with open(cfg_templ, 'wt') as f:
            f.write(cfg_template)

    # Package names need to be valid python identifiers
    # in the conda_build_config.yaml template. Conda
    # handles this by replacing hyphens with underscores.
    env = {pkg.replace('-', '_') : ver
           for pkg, ver in pinned_pkgs.items()}

    fprint(f'\nConda build config [{cfg_dest}]\n')
    fprint(render_template(cfg_templ, cfg_dest, **env))

    args = f'--variant-config-files {cfg_dest}'

    return args


def patch_meta_yaml(meta        : dict,
                    recipe_dir  : str,
                    pinned_pkgs : Dict[str, str]):
    """Applies global pathes to the project meta.yaml. The meta.yaml patches
    can be found in fsl_ci/templates/metayaml/.
    """

    if get_recipe_platform(meta) == 'noarch':
        return ''

    env = {pkg.replace('-', '_') : ver
           for pkg, ver in pinned_pkgs.items()}

    meta_file   = op.join(recipe_dir, 'meta.yaml')
    patch_dir   = op.join(op.dirname(__file__), '..', 'templates', 'metayaml')
    patch_files = sorted(glob.glob(op.join(patch_dir, '*.yaml')))

    shutil.copy(meta_file, f'{meta_file}.orig')

    meta = patch_meta_yaml_file(meta_file, meta_file, patch_files, env)

    fprint(f'\nRecipe meta.yaml [{meta_file}]\n\n{meta}\n')


def build_report(output_dir):
    """Lists all files produced by build_recipe."""
    for dirpath, _, filenames in os.walk(output_dir):
        for filename in filenames:
            filepath = op.join(dirpath, filename)
            size     = op.getsize(filepath) / 1048576
            fprint(f'{size:8.2f}MB {filepath}')


def patch_recipe(recipe_dir, project_repo, project_ref,
                 timestamp, server, token):
    """Patches the package version number in the recipe metadata.

    For development builds, a development suffix is added to the most recent
    stable project version number.

    :arg recipe_dir:   Directory to find the recipe meta.yaml
    :arg project_repo: Repository of project being being built
    :arg project_ref:  Git reference of project being built
    :arg timestamp:    YYYYMMDDHHMM timestamp to use in development version.
    :arg server:       GitLab server URL
    :arg token:        GitLab API access token
    """

    with open(op.join(recipe_dir, 'meta.yaml')) as f:
        meta = f.read()

    # Get project repository and version string
    # for most recent production release
    repo   = get_recipe_variable(meta, 'repository')
    ver    = get_recipe_variable(meta, 'version')
    gitref = None

    # override with FSLCONDA_REPOSITORY if set
    if project_repo is not None:
        repo = project_repo

    # if this is a FSL project, retrieve the git
    # revision and bake it into the version string
    # Dev packages are generally not built for
    # externally hosted projects, but the option
    # is there if we need it. See the
    # generate_devrelease_version function.
    if repo is not None and server in repo:
        project_path = urlparse.urlparse(repo).path.replace('.git', '')[1:]
        gitref = get_revision_hash(project_path, server, token, project_ref)

    ver  = generate_devrelease_version(ver, gitref, timestamp)
    meta = patch_recipe_version(meta, ver)

    with open(op.join(recipe_dir, 'meta.yaml'), 'wt') as f:
        f.write(meta)


def main():
    """Build a conda package from a FSL recipe repository.

    This script assumes that the following environment variables
    are set, in addition to the Gitlab CI predefined environment variables:

     - FSLCONDA_REVISION: Name of the git ref (e.g. tag, branch) to build the
       recipe from. If empty or unset, ref specified in the recipe meta.yaml
       file is used.

     - FSLCONDA_REPOSITORY: URL of the git repository to build the recipe
       from. If empty or unset, the repo specified in the recipe meta.yaml
       file is used.

     - DEVRELEASE: If equal to "true", the package is marked as a staging/
       development package.

     - FSLCONDA_INTERNAL: Marks this package as internal - if set, the
       FSLCONDA_INTENRAL_CHANNEL_URL will be added to the list of conda
       channels to source from, when building the package.

     - FSL_CI_API_TOKEN: GitLab API access token
    """

    recipe_url      = os.environ['CI_PROJECT_URL']
    recipe_ref      = os.environ['CI_COMMIT_REF_NAME']
    job_name        = os.environ['CI_JOB_NAME']
    server_url      = os.environ['CI_SERVER_URL']
    token           = os.environ['FSL_CI_API_TOKEN']
    pubchan         = os.environ['FSLCONDA_PUBLIC_CHANNEL_URL']
    devchan         = os.environ['FSLCONDA_DEVELOPMENT_CHANNEL_URL']
    intchan         = os.environ['FSLCONDA_INTERNAL_CHANNEL_URL']
    project_ref     = os.environ.get('FSLCONDA_REVISION',               '')
    project_repo    = os.environ.get('FSLCONDA_REPOSITORY',             '')
    manifest_ref    = os.environ.get('FSL_MANIFEST_REVISION',           None)
    manifest_repo   = os.environ.get('FSL_MANIFEST_REPOSITORY',         None)
    cfg_template    = os.environ.get('FSLCONDA_BUILD_CONFIG_TEMPLATE',  None)
    extra_args      = os.environ.get('FSLCONDA_BUILD_EXTRA_ARGS',       '')
    devrelease      = os.environ.get('DEVRELEASE', '').lower() == 'true'
    timestamp       = os.environ.get('DEVRELEASE_TIMESTAMP', None)
    internal        = 'FSLCONDA_INTERNAL' in os.environ
    username        = os.environ.get('FSLCONDA_INTERNAL_CHANNEL_USERNAME', None)
    password        = os.environ.get('FSLCONDA_INTERNAL_CHANNEL_PASSWORD', None)
    skip_platforms  = os.environ.get('FSLCONDA_SKIP_PLATFORM', '')
    skip_platforms  = skip_platforms.split()
    build_platforms = os.environ.get('FSLCONDA_BUILD_PLATFORM', '')
    build_platforms = build_platforms.split()

    if project_ref  == '': project_ref  = None
    if project_repo == '': project_repo = None

    # get the package name from the un-rendered metayaml text
    with open('meta.yaml', 'rt') as f:
        meta = f.read()
    package_name = get_recipe_variable(meta, 'name')

    # then load the rendered meta.yaml for use
    # by the get_platform_shortcut.. function
    # (we need the rendered meta.yaml because
    # e.g. CUDA package names are dynamically
    # generated with jinja2 templating).
    meta     = load_meta_yaml('meta.yaml')
    platform = get_platform_shortcut_if_not_applicable(
        meta, package_name, job_name, skip_platforms, build_platforms)

    # Get versions of core/base dependencies from the
    # fsl/conda/manifest:fsl-release.yml file - these
    # need to be pinned at build time.
    fsl_release = download_fsl_release(server_url, token,
                                       manifest_repo, manifest_ref)
    pinned_pkgs = get_build_package_versions(fsl_release, platform)

    # We direct builds for each platform type into a
    # separate sub-directory, because the outputs from
    # all builds will be forwarded to the deploy-package
    # job, and we want to avoid file collisions.
    #
    # conda has some seriously fucked up behaviour -
    # if you try to direct the build into a directory
    # called "noarch", it thinks that you are directing
    # it to a channel subdirectory, and explodes. So
    # we output to _platform_, rather thah platform.
    output_dir = op.join(os.getcwd(), 'conda_build', f'_{platform}_')

    if   internal:   destchan = intchan
    elif devrelease: destchan = devchan
    else:            destchan = pubchan

    if username is not None:
        intchan = add_credentials(intchan, username, password)
    if   internal:   channel_urls = [intchan, pubchan]
    elif devrelease: channel_urls = [devchan, pubchan]
    else:            channel_urls = [pubchan]

    os.makedirs(output_dir, exist_ok=True)

    fprint('************************************')
    fprint(f'Building conda recipe for:                 {package_name}')
    fprint(f'Recipe URL:                                {recipe_url}')
    fprint(f'Recipe revision:                           {recipe_ref}')
    fprint( 'Project repository (empty means to ')
    fprint(f'  build from repo specified in meta.yaml): {project_repo}')
    fprint( 'Project revision (empty means to ')
    fprint(f'  build release specified in meta.yaml):   {project_ref}')
    fprint(f'FSL conda channel URLs:                    {channel_urls}')
    fprint( 'Destination channel')
    fprint(f'  (will be published to):                  {destchan}')
    fprint('************************************')

    # patch the recipe so it has a dev version number
    if devrelease and project_ref is not None:
        patch_recipe('.', project_repo, project_ref, timestamp, server_url, token)

    build_recipe('.',
                 meta,
                 project_repo,
                 project_ref,
                 output_dir,
                 pinned_pkgs,
                 cfg_template,
                 extra_args,
                 *channel_urls)
    with indir(output_dir):
        build_report('.')


if __name__ == '__main__':
    main()
