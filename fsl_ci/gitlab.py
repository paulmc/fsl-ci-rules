#!/usr/bin/env python
#
# Functions for interacting with Gitlab over its HTTP REST API.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import                    json
import                    time
import                    datetime
import                    fnmatch
import base64          as b64
import functools       as ft
import urllib.parse    as urlparse
import urllib.request  as urlrequest

from fsl_ci.versioning import  is_valid_project_version
from fsl_ci            import (USERNAME,
                               EMAIL)


VERBOSE = True


def _print(msg, *args, **kwargs):
    if VERBOSE:
        msg = f'\x1b[90;1m{msg}\x1b[0m'
        print(msg, *args, **kwargs)


def add_params_to_url(url, **params):
    """Add GET parameters to url. """
    if len(params) == 0:
        return url

    parts    = list(urlparse.urlparse(url))
    parts[4] = dict(urlparse.parse_qsl(parts[4]))
    parts[4].update(params)
    parts[4] = urlparse.urlencode(parts[4])
    return urlparse.urlunparse(parts)


def gen_repository_url(project_path, server, token=None):
    """Generates a URL for the given project. """

    # we've been given a full URL to a git repo
    if project_path.startswith('http') or project_path.startswith('git'):
        return project_path

    if token is not None:
        token          = token.strip()
        prefix, suffix = server.split('://')
        server         = f'{prefix}://gitlab-ci-token:{token}@{suffix}'

    return f'{server}/{project_path}.git'


def gen_project_path(url):
    """Extract the project path from a repository URL. """
    if url.startswith('http'):
        path = urlparse.urlparse(url).path
        path = path.lstrip('/')
    elif url.startswith('git@'):
        path = url.split(':')[1]
    else:
        raise ValueError(f'unparseable: {url}')

    path = path.rstrip('/')
    path = path.removesuffix('.git')
    return path


def http_request(
        url,
        token=None,
        data=None,
        method=None,
        header=False,
        username=None,
        password=None,
        response_json=True):
    """Submit a HTTP request to the given URL. """

    if method is None:
        if data is None: method = 'GET'
        else:            method = 'POST'

    _print(f'{method} {url} ...')

    headers = {}

    if token is not None:
        headers['PRIVATE-TOKEN'] = token

    if data is not None:
        headers['Content-Type'] = 'application/json'
        data                    = json.dumps(data).encode('utf-8')

        _print(f'    payload: {data}')

    if username is not None:
        urlbase = urlparse.urlparse(url).netloc
        pwdmgr = urlrequest.HTTPPasswordMgrWithDefaultRealm()
        pwdmgr.add_password(None, urlbase, username, password)
        handler = urlrequest.HTTPBasicAuthHandler(pwdmgr)
        opener = urlrequest.build_opener(handler)
        opener.open(url)
        urlrequest.install_opener(opener)

    request  = urlrequest.Request(
        url, headers=headers, data=data, method=method)
    response = urlrequest.urlopen(request)
    payload  = response.read()

    if response_json:
        if len(payload) == 0: payload = {}
        else:                 payload = json.loads(payload)

    if header: return payload, response.info()
    else:      return payload


def http_request_all_pages(url, token):
    """Submit multiple requests to url, which is assumed to return
    paginated results.
    """
    results = []
    url     = add_params_to_url(url, per_page=100)

    while True:
        page, header = http_request(url, token=token, header=True)
        results.extend(page)
        url = parse_link_header(header, 'next')
        if url is None:
            break

    return results


def parse_link_header(header, get='next'):
    """Parses a linmk header, returning the URL corresponding to "get", or
    None if there is no such link/url.

    See https://docs.gitlab.com/ee/api/README.html#pagination
    """

    link = header.get('Link', None)

    if link is None:
        return None

    try:
        links = link.split(', ')

        for link in links:

            url, what = link.split('; ')

            if what == f'rel="{get}"':
                url = url.strip('<>')
                return url

    except Exception:
        pass
    return None


@ft.lru_cache()
def lookup_project_id(project_path, server, token):
    """Look up the integer ID of a gitlab project from its fully qualified
    path.
    """
    project_path = urlparse.quote_plus(project_path)
    url          = f'{server}/api/v4/projects/{project_path}'
    return http_request(url, token)['id']


def project_exists(project_path, server, token):
    """Returns true if the given project exists, false othewrise. """
    try:
        lookup_project_id(project_path, server, token)
        return True
    except Exception:
        return False


def lookup_namespace_id(namespace_path, server, token):
    """Look up the integer ID of a gitlab namespace from its fully qualified
    path.
    """
    url        = f'{server}/api/v4/namespaces'
    namespaces = http_request(url, token)

    for n in namespaces:
        if n['full_path'] == namespace_path:
            return n['id']

    raise ValueError(f'No namespace matching {namespace_path}')


def get_projects_in_namespace(namespace_path, server, token):
    """Returns a list of the paths of all projects in the given namespace.
    """
    nid      = lookup_namespace_id(namespace_path, server, token)
    url      = f'{server}/api/v4/groups/{nid}/projects?archived=False'
    projects = http_request_all_pages(url, token)
    projects = [p['path_with_namespace'] for p in projects]

    return projects


@ft.cache
def get_all_runners(server, token):
    """Get all specific runners available to the user."""
    url     = f'{server}/api/v4/runners'
    runners = http_request(url, token)
    return runners


def get_available_runners(project_path, server, token):
    """Returns a list of the IDs of all specific runners
    which are available to be used for the given project,
    and are not already enabled for it.
    """

    # remove inactive ones, and ones that are already
    # enabled
    runners = get_all_runners(server, token)
    runners = [r for r in runners if r['online'] and r['active']]
    runners = [r for r in runners if not runner_is_enabled(
        project_path, r['id'], server, token)]

    return [r['id'] for r in runners]


@ft.cache
def get_runner_metadata(runner_id, server, token):
    """Returns metadata for the specified runner. """
    url      = f'{server}/api/v4/runners/{runner_id}'
    response = http_request(url, token)
    return response


def get_runner_tags(runner_id, server, token, lower=True):
    """Returns the list of tags for the specified runner. """

    tags = get_runner_metadata(runner_id, server, token)['tag_list']

    if lower:
        tags = [t.lower() for t in tags]

    return tags


def find_suitable_runners(project_path, tags, server, token):
    """Identifies runners with the specified set of tags which
    are available to be used on the given project.
    """

    tags = [t.lower() for t in tags]

    def match(runner_tags):
        return all([t in runner_tags for t in tags])

    rids  = get_available_runners(project_path, server, token)
    rtags = [get_runner_tags(r, server, token) for r in rids]

    return [r for r, t in zip(rids, rtags) if match(t)]


def lookup_project_tags(project_path, server, token):
    """Return the a list of tags for the given project, or an empty list
    if the project has no tags.

    The tags are sorted such that the most recently updated tag is first
    in the list.
    """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/repository/tags'
    tags = http_request(url, token)
    tags = [t['name'] for t in tags]

    return tags


def get_tag_info(project_path, server, token, tag):
    """Get information about one tag. """
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/repository/tags/{tag}'
    return http_request(url)


def get_project_version(project_path, server, token):
    """Return the most recent version of the specified project, or None if
    the project has no tags, or if there are no tags that are a valid project
    version.
    """

    # tags ordered from newest to oldest
    tags = lookup_project_tags(project_path, server, token)

    for tag in tags:
        if is_valid_project_version(tag):
            return tag

    return None


def get_project_branches(project_path, server, token):
    """Returns a list of all branches of the project. """
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/repository/branches'
    return http_request_all_pages(url, token)


def list_project_branches(project_path, server, token):
    """Returns a list of all branch names of the project. """
    branches = get_project_branches(project_path, server, token)
    return [r['name'] for r in branches]


@ft.cache
def get_default_branch(project_path, server, token):
    """Returns the name of the default branch for the project, e.g. "master",
    "main" etc.
    """
    branches = get_project_branches(project_path, server, token)
    for b in branches:
        if b['default'] in (True, 'true', 'True'):
            return b['name']
    raise RuntimeError(f'Project {project_path} has no default branch')


def get_revision_hash(project_path, server, token, rev):
    """Returns the commit hash for the specified project revision, assumed to
    be a branch or tag name.
    """
    rev      = urlparse.quote_plus(rev)
    pid      = lookup_project_id(project_path, server, token)
    url      = f'{server}/api/v4/projects/{pid}/repository/commits?ref_name={rev}'
    commits  = http_request(url, token)
    return commits[0]['id']


def get_project_metadata(project_path, server, token):
    """Returns metadata for the specifie project."""
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/'
    return http_request(url, token)


def set_project_metadata(project_path, server, token, data):
    """Sets metadata for the specifie project."""
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/'
    http_request(url, token, data, method='PUT')

def get_protected_branches(project_path, server, token):
    """Returns a list of all protected branches for the given project. """
    pid      = lookup_project_id(project_path, server, token)
    url      = f'{server}/api/v4/projects/{pid}/protected_branches'
    response = http_request(url, token)
    return [r['name'] for r in response]


def protect_branch(project_path, branch, server, token):
    """Protects the specified branch, so that commits cannot be pushed
    directly to it.
    """
    pid = lookup_project_id(project_path, server, token)

    branches = list_project_branches(project_path, server, token)
    for b in branches:
        if fnmatch.fnmatch(b, branch):
            break
    else:
        return

    if branch in get_protected_branches(project_path, server, token):
        url = f'{server}/api/v4/projects/{pid}/protected_branches/{branch}'

    # GItlab does not let you change the settings
    # on a branch that is already protected, so
    # we first have to unprotect the branch.
    if branch in get_protected_branches(project_path, server, token):
        url = f'{server}/api/v4/projects/{pid}/protected_branches/{branch}'
        http_request(url, token, method='DELETE')

    url  = f'{server}/api/v4/projects/{pid}/protected_branches'
    data = {
        'name'                   : branch,
        'push_access_level'      : '0',
        'merge_access_level'     : '40',
        'unprotect_access_level' : '40',
    }
    http_request(url, token, data=data, method='POST')


def create_repository(project_path, server, token):
    """Create a repository on gitlab. """
    namespace, name = project_path.rsplit('/', 1)
    namespace       = lookup_namespace_id(namespace, server, token)
    url             = f'{server}/api/v4/projects'
    data            = {
        'name'         : name,
        'visibility'   : 'internal',
        'namespace_id' : namespace
    }
    http_request(url, token, data)


@ft.cache
def download_file(
        project_path, filename, server, token, ref=None, text=True):
    """Download the specified from the specified branch/ref of the project. """

    if ref is None:
        ref = get_default_branch(project_path, server, token)

    pid      = lookup_project_id(project_path, server, token)
    filename = urlparse.quote_plus(filename)
    url      = f'{server}/api/v4/projects/{pid}/repository/files/'
    url      = f'{url}/{filename}?ref={ref}'
    contents = http_request(url, token)['content']

    # contents are base64 encoded
    contents = b64.b64decode(contents)

    # bytes or str
    if text:
        contents = contents.decode('utf-8')

    return contents


def update_file(project_path,
                filename,
                contents,
                message,
                server,
                token,
                branch=None):
    """Update a file on the specified branch of the project. """

    if branch is None:
        branch = get_default_branch(project_path, server, token)

    pid      = lookup_project_id(project_path, server, token)
    filename =  urlparse.quote_plus(filename)
    url      = f'{server}/api/v4/projects/{pid}/repository/files/{filename}'
    data     = {
        'file_path'      : urlparse.quote_plus(filename),
        'content'        : contents,
        'branch'         : branch,
        'commit_message' : message,
        'author_name'    : USERNAME,
        'author_email'   : EMAIL,
    }
    http_request(url,  token, data=data, method='PUT')


def runner_is_enabled(project_path, runner_id, server, token):
    """Return True if the given runner is enabled for the given project,
    False otherwise.
    """
    runners = get_project_runners(project_path, server, token)
    rids    = [r['id'] for r in runners]

    return runner_id in rids


@ft.cache
def get_project_runners(project_path, server, token):
    """Return a list containing metadata for all enabled runners.
    """
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/runners'

    return http_request(url, token)


def enable_runner(project_path, runner_id, server, token):
    """Enables the specified runner on the specified project."""

    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/runners'
    data = {'runner_id' : runner_id}

    if not runner_is_enabled(project_path, runner_id, server, token):
        http_request(url, token, data)


def get_variables(project_path, server, token):
    """Returns a dict containing all environment variables set for the
    project.
    """
    pid      = lookup_project_id(project_path, server, token)
    url      = f'{server}/api/v4/projects/{pid}/variables'
    response = http_request(url, token)
    return {r['key'] : r['value'] for r in response}


def create_variable(project_path,
                    server,
                    token,
                    key,
                    value,
                    masked=False):
    """Creates a new variable on the project. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/variables'
    data = dict(key=key, value=value, masked=masked)
    http_request(url, token, data=data)


def update_variable(project_path,
                    server,
                    token,
                    key,
                    value,
                    masked=False):
    """Updates the value of a variable on the project. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/variables/{key}'
    data = {'value' : value, 'masked' : masked}
    http_request(url, token, data=data, method='PUT')


def create_or_update_variable(project_path,
                              server,
                              token,
                              key,
                              value,
                              **kwargs):
    """Create or update a variable on the project. """
    if key in get_variables(project_path, server, token):
        method = update_variable
    else:
        method = create_variable
    method(project_path, server, token, key, value, **kwargs)


def delete_variable(project_path, server, token, key):
    """Delete a variable from the project. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/variables/{key}'
    http_request(url, token, method='DELETE')


def create_branch(project_path,
                  branch_name,
                  source_branch,
                  server,
                  token):
    """Create a new branch on the project, from the source branch/ref. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/repository/branches'
    data = {
        'branch' : branch_name,
        'ref'    : source_branch,
    }
    http_request(url, token, data)


def open_issue(project_path,
               title,
               description,
               server,
               token):
    """Open an issue on the given project. """

    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/issues'
    data = {
        'id'                   : pid,
        'title'                : title,
        'description'          : description,
    }
    return http_request(url, token, data=data)


def open_merge_request(project_path,
                       source_branch,
                       message,
                       server,
                       token,
                       target_branch=None,
                       title=None):
    """Opens a merge request from the source branch to the target branch,
    on the given project.
    """
    if target_branch is None:
        target_branch = get_default_branch(project_path, server, token)
    if title is None:
        title = f'WIP: {source_branch}'

    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/merge_requests'
    data = {
        'id'                   : pid,
        'source_branch'        : source_branch,
        'target_branch'        : target_branch,
        'remove_source_branch' : 'true',
        'title'                : title,
        'description'          : message,
    }
    return http_request(url, token, data=data)


def list_merge_requests(project_path, server, token, from_=None, to=None, state=None):
    """List all merge requests"""

    # Not currently possible to filter
    # MRS by their *merge* date, only
    # by their creation date :(.
    params = {}
    if from_ is not None: params['created_after']  = from_
    if to    is not None: params['created_before'] = to
    if state is not None: params['state']          = state

    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/merge_requests'
    url = add_params_to_url(url, **params)

    def filter_by_date(mrs, date, field, direction):
        filtered = []
        date     = parse_gitlab_date(date)
        for mr in mrs:
            mrdate = mr[field]
            if mrdate is None:
                filtered.append(mr)
                continue
            mrdate = parse_gitlab_date(mr[field])
            if   direction == '>' and mrdate > date:
                filtered.append(mr)
            elif direction == '<' and mrdate < date:
                filtered.append(mr)
        return filtered

    mrs = http_request_all_pages(url, token)

    if from_ is not None: mrs = filter_by_date(mrs, from_, 'merged_at', '>')
    if to    is not None: mrs = filter_by_date(mrs, to,    'merged_at', '<')

    return mrs


def get_merge_request(project_path, server, token, mrid=None, commit=None):
    """Get information about either a specific merge request (mrid), or
    the merge request associated with a specific commit hash.
    """
    if (mrid is not None) and (commit is not None):
        raise ValueError('Only one of mrid and commit may be specified')

    pid = lookup_project_id(project_path, server, token)

    if mrid is not None:
        url = f'{server}/api/v4/projects/{pid}/merge_requests/{mrid}'
        return http_request(url, token, method='GET')

    else:
        url = f'{server}/api/v4/projects/{pid}/repository/commits/{commit}/merge_requests'
        resp = http_request(url, token, method='GET')

        if len(resp) == 0: return None
        else:              return resp[0]


def merge_merge_request(project_path, mrid, server, token):
    """Merge the merge request with the specified ID. """

    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/merge_requests/{mrid}/merge'
    return http_request(url, token, method='PUT')


def trigger_pipeline(project_path,
                     ref,
                     server,
                     token,
                     variables=None):
    """Triggers a CI pipeline on the given project/ref. Returns the pipeline
    integer ID."""
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/pipeline'
    data = {'ref' : ref}

    if variables is not None:
        data['variables'] = [{'key'           : k,
                              'variable_type' : 'env_var',
                              'value'         : v}
                             for k, v in variables.items()]
    return http_request(url, token, data)


def list_pipelines(project_path, server, token, ref=None):
    """Return a list of the last 20 pipelines for the given project. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/pipelines'
    if ref is None: data = None
    else:           data = {'ref' : ref}
    # I'm assuming that pipeline entries are sorted descending
    return http_request(url, token, method='GET', data=data)


def cancel_pipeline(project_path, pipeline_id, server, token):
    """Cancel the pipeline with the specified ID. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/pipelines/{pipeline_id}/cancel'
    return http_request(url, token, method='POST')


def get_pipeline_status(project_path, pipeline_id, server, token):
    """Returns information about the given pipeline. """
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/pipelines/{pipeline_id}'
    return http_request(url, token)


def wait_on_pipeline(project_path, pipeline_id, server, token,
                     verbose=True, timeout=60):
    """Waits until the given pipeline finishes, returning its final status. """

    start = time.time()

    while True:
        secs = int(time.time() - start)
        time.sleep(timeout)
        if verbose:
            _print(f'[{project_path}] {secs} seconds elapsed...', flush=True)

        status = get_pipeline_status(
            project_path, pipeline_id, server, token)['status']

        if status in ('success', 'failed', 'canceled', 'skipped', 'manual'):
            _print(f'Pipeline has stopped - final status: {status}')
            break

        if verbose:
            _print(f'Pipeline status: {status}')

    return status


def get_project_jobs(project_path, server, token, scope=None, page=1):
    """Returns a list of the most recent pipeline jobs that have submitted for
    the given project.
    """
    # jobs are sorted descending, so
    # we only need the first page
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/jobs?page={page}'

    if scope is not None:
        url = f'{url}&scope={scope}'
    return http_request(url, token)


def find_latest_job(
        project_path, server, token, jobpat=None, age=None, pid=None):
    """Return info about the most recent CI jobs with the following
    optional criteria:
      - a name matching the specified ``jobpat`` (a fnmatch-style wildcard)
      - which was submitted at most ``age`` hours ago
      - which are associated with the specified pipeline ID ``pid``.
    """
    now   = datetime.datetime.now().astimezone()
    jobs  = get_project_jobs(project_path, server, token)
    found = {}

    for job in jobs:
        name     = job['name']
        created  = parse_gitlab_date(job['created_at'])
        timediff = (now - created).total_seconds() / 3600
        match    = fnmatch.fnmatch(name, jobpat)
        pipeline = job['pipeline']['id']

        # only return the most recent of each
        # uniquely named job (get_project_jobs
        # returns jobs sorted by ID, meaning
        # that they are also sorted by creation
        # time)
        if name in found:
            continue

        # apply selectors
        if (pid    is not None) and (pipeline != pid): continue
        if (jobpat is not None) and (not match):       continue
        if (age    is not None) and (timediff > age):  continue

        _print(f'Found job {name} [ID {job["id"]}] '
               f'scheduled {timediff} hours ago')
        found[name] = job

    return list(found.values())


def trigger_job(project_path, job_id, server, token):
    """Start the specified CI job."""
    pid = lookup_project_id(project_path, server, token)
    url = f'{server}/api/v4/projects/{pid}/jobs/{job_id}/play'
    return http_request(url, token, method='POST')


def parse_gitlab_date(datestr):
    """Parse a gitlab timestamp, returning a Python datetime object.
    2020-11-13T15:13:48.649+01:00
    """
    timestamp = datestr.split('.')[0]
    offset    = datestr.split('+')[1]

    if offset == '':
        offset = 0
    else:
        offset = (datetime.datetime.strptime(offset, '%H:%M') -
                  datetime.datetime(1900, 1, 1, 0, 0)).seconds / 3600.0

    timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
    offset    = datetime.timezone(datetime.timedelta(hours=offset))
    return timestamp.replace(tzinfo=offset)


def format_gitlab_date(datestamp):
    """Format a python datetime object in a gitlab compatible format.
    """
    return datestamp.isoformat()


def get_divergence(project_path, server, token, ref1, ref2):
    """Get the commits that ref1 is ahead and behind ref2. """

    pid  = lookup_project_id(project_path, server, token)
    ref1 = urlparse.quote_plus(ref1)
    ref2 = urlparse.quote_plus(ref2)

    url1    = f'{server}/api/v4/projects/{pid}/repository/compare' + \
              f'?from={ref1}&to={ref2}'
    url2    = f'{server}/api/v4/projects/{pid}/repository/compare' + \
              f'?from={ref2}&to={ref1}'

    ahead   = http_request(url1, token)['commits']
    behind  = http_request(url2, token)['commits']

    return ahead, behind


def gen_branch_name(branch_name_base, project_path, server, token):
    """Generates a unique branch name on a project repository. """

    allbranches = list_project_branches(project_path, server, token)
    count       = 0
    branch      = branch_name_base

    while branch in allbranches:
        count += 1
        branch = f'{branch_name_base}-{count}'

    return branch


def download_archive(project_path, server, token, rev=None):
    """Download a ZIP archive for a specific revision. """
    pid  = lookup_project_id(project_path, server, token)
    url  = f'{server}/api/v4/projects/{pid}/repository/archive.zip'

    if rev is not None:
        url = f'{url}?sha={rev}'

    return http_request(url, token, response_json=False)
