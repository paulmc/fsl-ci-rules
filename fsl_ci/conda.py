#!/usr/bin/env python
#
# Functions for working with FSL conda recipe repositories, and with conda
# channels.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import functools    as     ft
import os.path      as     op
import                     os
import                     re
import                     bisect
import                     dataclasses
import                     string
import                     shutil
import urllib.parse as     urlparse
from   unittest     import mock

from typing import Dict, List, Union, Tuple, Optional

from fsl_ci        import loadyaml, fprint
from fsl_ci.recipe import get_recipe_variable
from fsl_ci.gitlab import http_request, download_file


def condabin(consider_mamba=True):
    """Returns the path to the conda or mamba command. """

    candidates = []

    onpath = [shutil.which('conda')]
    if consider_mamba:
        onpath.insert(0, shutil.which('mamba'))

    for cmd in onpath:
        if cmd is not None:
            candidates.append(cmd)

    for condabin in candidates:
        if op.exists(condabin):
            return condabin

    return 'conda'


def gen_recipe_path(project_path):
    """Generates a conda recipe project name from the given project.

    If the project name has one of the following forms (case doesn't matter):
      - "fsl-<project>"
      - "fsl_<project>"
      - "fsl<project>"

    the recipe name will be "fsl-<project>". Otherwise, the recipe name will
    be "fsl-<project_name>". In both cases, the recipe name will be all lower
    case.

    The full recipe path will be "fsl/conda/<recipe_name>"
    """

    # strip namespaced in case
    # fsl/<project> was passed in
    project_name = project_path.rsplit('/')[-1]
    match = re.match(r'^(fsl[_-]?)?(.*)$', project_name, flags=re.IGNORECASE)
    suffix = match.group(2).lower()

    return f'fsl/conda/fsl-{suffix}'


def get_recipe_urls(project_name, server) -> Dict[str, str]:
    """Figures out the URLs to the conda recipes associated with the specified
    FSL project. Most FSL projects have a single recipe associated with them,
    but there are a few projects which are associated with multiple recipes,
    so this function returns a list.

    Each item in the returned list is a dict describing one recipe repository,
    with keys 'url', 'name' and 'path'.
    """


    # Figure out the name of the conda recipe associated with
    # this project. It defaults to "fsl-<project>", but can
    # be overridden via the FSLCONDA_RECIPE_URL environment
    # variable.
    urls         = os.environ.get('FSLCONDA_RECIPE_URL')
    project_name = project_name.rsplit('/')[-1]

    if urls is not None:
        urls = urls.split()
    else:
        path = gen_recipe_path(project_name)
        urls = [f'{server}/{path}.git']

    recipes = []

    for url in urls:
        url  = url.rstrip('/')
        path = urlparse.urlparse(url).path.replace('.git', '')[1:]
        name = path.rsplit('/', 1)[-1]
        recipes.append({'url' : url, 'name' : name, 'path' : path})

    return recipes


def is_standard_recipe_path(project_path, recipe_path):
    """Returns True if the given recipe path follows the "standard" convention
    of being "fsl/conda/fsl-<project_name>", False otherwise.
    """
    project_name = project_path.rsplit('/')[-1].lower()
    return recipe_path == f'fsl/conda/fsl-{project_name}'


def load_meta_yaml(filename, ignore_env=False):
    """Load a conda recipe meta.yaml file. "filename" may be a path to
    the meta.yaml file, or may be the contents of a file.

    If ignore_env is True, the rendered template will not be affected
    by any environment variables in os.environ.
    """

    import jinja2 as j2

    def nil_func(*args, **kwargs):
        return "nil"

    class OS:
        def __init__(self):
            self.environ = {}

    template_os = OS()

    if not ignore_env:
        template_os.environ = os.environ.copy()

    if 'CUDA_VER' not in template_os.environ:
        template_os.environ['CUDA_VER'] = 'X.Y'

    # symbols and macros that may be
    # in recipes, but which we don't
    # use/care about.
    env = {
        'os'                 : template_os,
        'load_setup_py_data' : lambda : {},
        'compiler'           : nil_func,
        'cdt'                : nil_func,
        'pin_compatible'     : lambda pkg: pkg,
        'pin_subpackage'     : nil_func,
    }

    if op.exists(filename):
        with open(filename, 'rt') as f:
            template = j2.Template(f.read())
    else:
        template = j2.Template(filename)

    meta = template.render(**env)
    meta = loadyaml(meta)

    return meta


def get_project_repository_and_revision(filename, ignore_env=False):
    """Returns a tuple containing the project git repository URL, git
    revision, and build number listed in the given meta.yaml file. If
    repository, revision, or build are not in the metadata, the
    respective returned value will be None.

    If ignore_env is True, the FSLCONDA_REPOSITORY and FSLCONDA_variables
    are ignored, if set.

    The values are obtained as follows:
      - The build number is taken from the `build: number` field
      - The git repository is taken from the `source: git_url` field. If this
        field is not present, the repository is taken form the `repository`
        jinja2 variable, if set.
      - The git revision is taken from the `source: git_rev` field. If this
        field is not present, the repository is taken form the `version`
        jinja2 variable, if set.
    """

    env_repo = os.environ.get('FSLCONDA_REPOSITORY', '').strip()
    env_rev  = os.environ.get('FSLCONDA_REVISION',   '').strip()

    if env_repo == '': env_repo = None
    if env_rev  == '': env_rev  = None

    with mock.patch.dict(os.environ):
        # Remove env vars if ignore_env is set. If
        # the env vars are set, but are empty, this
        # will have the effect that the relevant
        # entries in the rendered meta.yaml will
        # be returned as None. So in this case we
        # remove them from the env (they are
        # initialised to empty by the CI build rule,
        # with the intent that empty is interpreted
        # as unset).
        if ignore_env or (env_rev is None):
            os.environ.pop('FSLCONDA_REVISION',   None)
        if ignore_env or (env_repo is None):
            os.environ.pop('FSLCONDA_REPOSITORY', None)

        with open(filename, 'rt') as f:
            metatxt = f.read()
        meta = load_meta_yaml(metatxt)

    source = meta  .get('source',  {})
    build  = meta  .get('build',   {})
    repo   = source.get('git_url', None)
    rev    = source.get('git_rev', None)
    build  = build .get('number',  None)

    if repo is None: repo = get_recipe_variable(metatxt, 'repository')
    if rev  is None: rev  = get_recipe_variable(metatxt, 'version')

    # if the repo/rev were taken from the source section,
    # the FSLCONDA_* vars will have already been applied.
    # But they won't have been applied if the repo/rev
    # were taken from repository/version jinja2 variables.
    if (not ignore_env) and (env_repo is not None): repo = env_repo
    if (not ignore_env) and (env_rev  is not None): rev  = env_rev

    if repo   is not None: repo  = str(repo) .strip()
    if rev    is not None: rev   = str(rev)  .strip()
    if build  is not None: build = str(build).strip()
    if repo   == '':       repo  = None
    if rev    == '':       rev   = None
    if build  == '':       build = None

    return repo, rev, build


@ft.total_ordering
class Version:
    """Class for parsing/comparing version strings. """
    def __init__(self, verstr):
        self.verstr = verstr

        parts = []

        verstr = verstr.lower()
        if verstr.startswith('v'):
            verstr = verstr[1:]

        for part in verstr.split('.'):

            # FSL development releases may have ".postN"
            if part.startswith('post'):
                part = part[4:]
            # FSL development releases may have ".devYYYYMMDD<githash>"
            if part.startswith('dev'):
                for end, char in enumerate(part[3:], 3):
                    if char not in string.digits:
                        break
                part = part[3:end]
            try:
                parts.append(int(part))
            except Exception:
                break
        self.parsed_version = tuple(parts)

    def __lt__(self, other):
        return self.parsed_version < other.parsed_version

    def __eq__(self, other):
        return self.parsed_version == other.parsed_version


@ft.total_ordering
@dataclasses.dataclass
class Package:

    name : str
    """Package base name."""

    version : str
    """Version string. We assume that the same versions are available for all
    supported platforms.
    """

    build : int
    """Build number. We assume that the same builds are available for all
    supported platforms.
    """

    platforms : List[str]
    """List of platforms (e.g. "noarch", "osx-64") for which this package
    is available.
    """

    dependencies : List[Union[str, 'Package']]
    """References to all packages which this package depends on.  Stored
    as a reference to another Package object for other packages hosted
    on the same channel, or a "package [version-constraint]" string for
    packages hosted elsewhere.
    """

    recipe_path : Optional[str]
    """Path of gitlab recipe repository, if internally hosted. """


    @property
    def development(self) -> bool:
        """Return True if this is a development version of the package. """
        return 'dev' in self.version


    def __lt__(self, pkg):
        """Only valid when comparing another Package with the same name and
        platform.
        """

        selfver = Version(self.version)
        pkgver  = Version(pkg.version)

        if     selfver    < pkgver: return True
        if     selfver    > pkgver: return False
        return self.build < pkg.build


    def __eq__(self, pkg):
        """Only valid when comparing another Package with the same name and
        platform.
        """
        return ((Version(self.version) == Version(pkg.version)) and
                (self.build            == pkg.build))


def get_channel_packages(channel_url : str, **kwargs) -> Dict[str, List[Package]]:
    """Returns a list of packages hosted at the given channel. """
    channeldata, platformdata = read_channel_repodata(channel_url, **kwargs)
    packages = load_packages(channeldata, platformdata)
    return packages


def read_channel_repodata(channel_url : str, **kwargs) -> Tuple[Dict, Dict]:
    """Loads channel and platform metadata from the channel. """

    # load channel and platform metadata - the
    # first gives us a list of all packages that
    # are hosted on the channel, and the second
    # gives us the dependencies of each package.
    channeldata  = http_request(f'{channel_url}/channeldata.json', **kwargs)
    platformdata = {}
    for platform in channeldata['subdirs']:
        url = f'{channel_url}/{platform}/repodata.json'
        platformdata[platform] = http_request(url)

    return channeldata, platformdata


def load_packages(channeldata  : Dict,
                  platformdata : Dict,
                  linked       : bool = True) -> Dict[str, List[Package]]:
    """Creates a Package object for every package hosted in the channel.
    The returned dictionary contains {pkgname : [Package]} mappings
    where, for each package hosted on the channel, the value is a list
    of Package objects, one for each available version.  Each package
    list is sorted by its version number, with the oldest version first,
    and the most recent version last.
    """

    packages = {}

    # The channeldata.json file contains an entry for
    # every package hosted on the channel, and gives
    # the version number of the latest available
    # version.
    #
    # We do one pass through channeldata to create a
    # Package object for the latest version of every
    # package in the channel, but without
    # dependencies - we'll gather the other versions
    # and fill in dependencies in a second pass
    # through the platform-specific repodata below.
    #
    # We also update the build number in the second pass
    for name, meta in channeldata['packages'].items():
        version        = meta['version']
        platforms      = meta['subdirs']
        packages[name] = [Package(name, version, 0, platforms, [], None)]

    # We assume that binary packages are available for
    # all platforms (linux-64, macos-64, ..), so we
    # only look through the noarch and linux-64
    # package lists.
    pkgfiles = dict(platformdata['noarch']['packages'])
    pkgfiles.update(platformdata['linux-64']['packages'])

    def find_builds(name):
        builds = []
        for meta in pkgfiles.values():
            if meta['name'] == name:
                builds.append(meta)

        def key(meta):
            ver   = Version(meta['version'])
            build = meta['build_number']
            return ver, build
        return sorted(builds, key=key, reverse=True)

    # Load dependency information - this is stored
    # in platform-specific repodata.json files.
    for name, pkgs in packages.items():

        builds = find_builds(name)

        # set build number and dependencies
        # on the newest Package object
        pkgs[-1].build = builds[0]['build_number']

        for dep in builds[0]['depends']:
            # The entry for each package file contains a list
            # of "package [version-constraint]" strings.
            name = dep.split()[0]

            # Externally hosted package - store
            # the full dependency string
            if (not linked) or (name not in packages):
                pkgs[-1].dependencies.append(dep)

            # Another package hosted in this
            # channel - store a ref to the
            # (latest) Package object
            else:
                pkgs[-1].dependencies.append(packages[name][-1])

        # add Package entry for all other builds
        for build in builds[1:]:

            pkg = Package(build['name'],
                          build['version'],
                          build['build_number'],
                          pkgs[-1].platforms,
                          build['depends'],
                          None)
            bisect.insort(pkgs, pkg)

    return packages


def load_packages_from_recipe_repositories(repos  : List[str],
                                           server : str,
                                           token  : str) -> Dict[str, Package]:
    """Create a package object for every recipe repository. """

    packages = {}

    # The channeldata.json file contains an entry for
    # every package hosted on the channel. We use this
    # to create a Package object for every package in
    # the channel, but without dependencies - we'll do
    # this in a second pass below.
    for repo in repos:
        meta    = download_file(repo, 'meta.yaml', server, token)
        meta    = load_meta_yaml(meta)
        name    = meta['package']['name']
        version = meta['package']['version']
        build   = meta['build']['number']

        # lump host/build/run requirements together
        deps    = meta.get('requirements', {})
        deps    = deps.get('host',         []) + \
                  deps.get('build',        []) + \
                  deps.get('run',          [])
        deps    = sorted(set(deps))

        # don't have info to fully populate platforms
        plats   = []
        if meta['build'].get('noarch', False):
            plats.append('noarch')

        packages[name] = Package(name, version, build, plats, deps, repo)

    # pass 2 - resolve dependencies
    for name, pkg in packages.items():
        for i, dep in enumerate(pkg.dependencies):
            dep = dep.split()[0]

            if dep in packages:
                pkg.dependencies[i] = packages[dep]
    return packages


def build_dependency_graph(packages : Dict[str, Package]):
    """Builds a directed dependency graph from the given collection of
    packages. Externally hosted packages are not included in the graph.
    """

    import networkx as nx

    g = nx.DiGraph()

    # nodes
    for pkg in packages.values():
        g.add_node(pkg.name)

    # edges
    for pkg in packages.values():
        for dep in pkg.dependencies:
            if isinstance(dep, Package):
                g.add_edge(pkg.name, dep.name)

    return g


def find_dependant_packages(package  : str,
                            all_pkgs : Dict[str, List[Package]]
                            ) -> List[str]:
    """Find all packages which list package as a dependency. all_pkgs is
    assumed to be a dictionary returned by the load_packages function.
    """
    dependants = set()

    for pkg in all_pkgs.values():
        pkg = pkg[-1]
        if isinstance(pkg, str):
            continue

        pkgname = pkg.name
        cudapkg = re.match(r'(.*-cuda)-(\d+\.\d+|X\.Y)$', pkgname)
        if cudapkg:
            pkgname = cudapkg.group(1)

        for dep in pkg.dependencies:
            if not isinstance(dep, str):
                dep = dep.name
            if dep == package:
                dependants.add(pkgname)
                break
    return list(dependants)


def sort_packages_by_dependence(
        pkgs : Dict[str, Package]) -> List[List[Package]]:
    """Sorts projects according to their position in the dependency hierarchy.
    Returned as a list of lists, where each child list contains a set of
    packages that do not depend on each other, and thus can be built in
    parallel.

    The returned list of groups will not be accurate if the packages list is
    under-specified i.e. there is a gap in a dependency chain.
    """

    allpkgs = set(pkgs.keys())

    def get_next_build_group(pkgs, previous):
        group = []
        for name, pkg in pkgs.items():

            # Only consider dependencies in the
            # set of packages we have been given
            reqs = {getattr(r, 'name', r) for r in pkg.dependencies}
            reqs = reqs.intersection(allpkgs)

            if len(reqs.difference(previous)) == 0 and \
               (name not in previous)              and \
               (name not in group):
                group.append(name)
        return group

    def get_build_group_order(pkgs):
        groups   = []
        previous = []
        while len(previous) != len(pkgs):
            group = get_next_build_group(pkgs, previous)
            groups  .append(group)
            previous.extend(group)
        return groups

    groups   = get_build_group_order(pkgs)
    projects = []

    for group in groups:
        projects.append([])
        for pkgname in group:
            projects[-1].append(pkgs[pkgname])

    return projects
