# CI rules for internally hosted FSL projects


This project hosts Gitlab CI automation routines for internally hosted FSL
projects. These routines are hosted at
https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules, and are applied to most FSL
project repositories.


This project also contains `Dockerfile` definitions for Docker images used to
build Linux, CUDA, and platform-independent conda packages for FSL projects.


Every FSL project comprises **two** git repositories:


 * The _project_ repository contains the project source code/resources, and is
   typically hosted at `https://git.fmrib.ox.ac.uk/fsl/<project>`.
 * The _recipe_ repository contains a conda recipe for the project, and is
   typically hosted at
   `https://git.fmrib.ox.ac.uk/fsl/conda/fsl-<project>`<sup>*</sup>.


_This_ repository contains CI rules which are applied to both project and
recipe repositories to facilitate automatic testing, and building and
deployment of FSL conda packages.


> <sup>*</sup>A small number of FSL projects have more than one recipe
> repository associated with them - for example, the
> [`fsl/fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt) project has two recipe - the
> [`fsl/conda/fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt) recipe
> provides CPU executables, and the
> [`fsl/conda/fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda)
> recipe provides GPU/CUDA executables.


**Important:** The CI rules are written under the assumption that the
following conventions hold. If these conventions are not followed, some rules
will not function correctly:


 - The FSL **project** repository is hosted at
   `https://git.fmrib.ox.ac.uk/fsl/<project>`.
 -  The accompanying conda **recipe** repository is hosted at
   `https://git.fmrib.ox.ac.uk/fsl/conda/fsl-<project>`, with `<project>`
   comprised solely of lowercase alpha characters, numeric digits,
   underscores, hyphens, or dots.
 - The conda package name (as defined in the recipe `meta.yaml` file) is
   `fsl-<project>`, with `<project>` all lower-case.
 - New stable releases are created as **tags** on the `master`/`main` branch
   of the FSL **project** repository. A tag is used as the version number for
   a project, and must comprise a sequence of monotonically increasing
   integers, separated with dots, and with an optional leading `v`.


**Important:** More documentation regarding FSL conda packaging instructure
can be found at fsl/conda/docs>.


## Overview


When a new release of a FSL project is tagged on the project repository:

 1. A merge request is opened on the conda recipe for the project, updating
    the version number in the recipe.

 2. When that merge request is merged, the conda recipe is re-built against
    the new version.

 3. Any `pyfeeds` tests contained in the repository are executed.

 4. The resulting conda package is uploaded to a _public_ conda channel,
    which forms the basis for public FSL releases.

 5. A merge request is opened on the fsl/conda/manifest> repository, updating
    the package version number in the FSL release manifest.


Conda packages for certain FSL projects are not uploaded to the _public_ conda
channel, but rather are uploaded to a separate _internal_ conda channel,
which is only accessible by authorised users.

*Development*, or *pre-release* packages of any FSL project may optionally be
built from any branch of the FSL project repository by manually running the
`trigger-devrelease-package-build` CI job. These packages will be published to
the _development_ conda channel.


The FSL conda channels are currently hosted at:
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal/ (login required)


> **Note:** All package uploads must be triggered manually, by a project
> maintainer, on the recipe repository via the Gitlab web interface.


### Organisation

This repository is organised as follows:

 - The `rules` directory contains YAML files which specify all of the CI jobs
   that are executed - these are included by the main `.gitlab-ci.yml` file
   (which itself just contains some initialisation logic common to all jobs).
 - The `fsl_ci` directory contains python scripts and utilities which are
   called by the CI jobs, and some utility scripts that can be manually
   invoked to perform various tasks - see below.
 - The `docker` directory contains `Dockerfile` definitions for custom Docker
   images which are used for certain jobs.


## Bootstrapping a new project


The `fsl-ci-rules` repository contains a number of Python utilities which can
be used to bootstrap CI and conda integration for new or un-configured
projects.  These scripts are intended to be executed manually by a FSL
maintainer - they should only need to be executed once. The scripts require
Python 3.7, `pyyaml`, `jinja2`, and `networkx`, and the `create_conda_recipe`
script works best if run in an environment with an active FSL installation,
so using `fslpython` is a good option.


 - `create_conda_recipe`: Creates an initial conda recipe repository for a FSL
   project repository from a standard template (see
   `fsl_ci/utils/templates/`).
 - `configure_repositories`: Automatically configure a project and recipe
   repository to use the `fsl-ci-rules`.
 - `trigger_build`: Automatically trigger a staging or production package build
    on a recipe repository, and deploy it to the relevant channel.


These utilities are provided as a Python package - you can install them into
a Python environment using `pip`, e.g.:


```
python -m pip git+https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules.git
```


### Example


> **Note:** The steps below are a suggestion. If you are comfortable working
> with git repositories, conda environments, and building conda packages by
> hand, see the help documentation for the `create_conda_recipe.py` script,
> and feel free to work in whichever way you see fit.


Let's say we have developed a new FSL project at
`https://git.fmrib.ox.ac.uk/fsl/my_cool_project`. To integrate our new
project into the FSL conda and CI ecosystem, we can follow these steps:


1. Make sure that you have the latest FSL installed. This is not strictly
   necessary, but will give you a suitable Python environment to use and, for
   C++ projects, will allow the dependencies of your project to be identified
   more accurately.

2. If you haven't already done so, create a Gitlab API access token with
   read+write ("`api`") access. This can be done via the Gitlab web interface
   via _User settings -> Access Tokens_.

3. Install the `fsl-ci-rules` utilities:

        fslpython -m pip install git+https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules.git

4. Create a conda recipe repository for the new project - this step will
   create a local git repository at `./recipe`, configured to push to a GitLab
   repository at `https://git.fmrib.ox.ac.uk/fsl/conda/fsl-my_cool_project`:

        create_conda_recipe -t <token> -pp fsl/my_cool_project ./recipe

5. Configure the project and recipe repositories - this will enable
   integration with the `fsl-ci-rules` CI configuration on both repositories:

        configure_repositories -t <token> \
            fsl/my_cool_project \
            fsl/conda/fsl-my_cool_project

6. To test that the CI integration is working:
   - Trigger a pipeline on the `master`/`main` branch of the
     `fsl/my_cool_project` repository via the Gitlab web interface (_CI / CD
     -> Run Pipeline_), or via the `trigger_pipeline` command.
   - Create a tag on the `fsl/my_cool_project` repository.

7. Manually edit the recipe at the `fsl/conda/fsl-my_cool_project` repository
   until your conda package builds successfully. You will probably have to
   adjust the requirements of your project, as the routines used to
   automatically identify requirements are not perfect.


## Other utilities

The `fsl-ci-rules` utilities contain a handful of other utility scripts which
can be useful for manually managing recipes and builds from the command-line.

 - `package_status`: Print a table containing the status of all FSL conda
    recipes contained in the Gitlab `fsl/conda/` namespace, including the
    latest versions published on the staging and production channels, and the
    most recent tags on the corresponding project repository.
 - `trigger_build`: Trigger a new build on one or more FSL conda recipes, wait
   for them to complete, and then trigger the `deploy-conda-package` job to
   deploy the new build to the production or staging channel. This script is
   useful when a recipe has not changed, but needs to be re-built, for example
   if the build environment has changed.
 - `trigger_pipeline`: Trigger a pipeline on a specific branch of any
   repository, or trigger a manual job by name.
 - `set_gitlab_variables`: Automatically set variables on one or more gitlab
   repositories.

Most of these utilities interact with Gitlab via its REST API, so an access
token will need to be defined - a useful strategy is to store your token in
an envirohnment variable called e.g. `$TOKEN`:


```
export TOKEN="<your-gitlab-access-token>"
trigger_build -t "$TOKEN" ...
```


## Conda package platforms/architectures


FSL conda packages are built for the following platforms/architectures:

 - `noarch`:            Platform-independent, e.g. Python, TCL, Bash projects
 - `linux-64`:          C/C++ projects compiled for Linux (x86-64)
 - `linux-aarch64`:     C/C++ projects compiled for Linux (arm-64)
 - `macos-64`:          C/C++ projects compiled for macOS (x86-64)
 - `macos-M1`:          C/C++ projects compiled for macOS (M1)
 - `linux-64-cuda-X.Y`: CUDA projects compiled for Linux (x86-64), with CUDA
                        Toolkit version X.Y.


> *Note*: The above platform labels are used as identifiers - they must be used
> exactly as shown.


When a conda package build is triggered on any recipe repository, separate
build jobs will be started for **all** of the above platforms. The GitLab CI
jobs which are executed for each platform are defined in the
`rules/fsl-ci-build-rules.yml` file - all of these jobs invoke the
`build_conda_package` command. This script is configured to check the
requested platform, and to check the type of the project being built, and to
immediately abort unnecessary builds.


For example, the
[`fsl/conda/fsl-add_module`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-add_module)
recipe repository is a recipe for the
[`fsl/add_module`](https://git.fmrib.ox.ac.uk/fsl/add_module) project, which
is a pure Python project that is built as a `noarch` package. When a build is
triggered on the `fsl/conda/fsl-add_module` repository, jobs for `noarch`,
`linux-64`, `linux-aarch64`, `macos-64`, `macos-M1`, and supported `cuda`
variants will all be started. However, only the `noarch` job will proceed -
all of the other jobs will detect that they are unnecessary, and will
immediately abort. The `build_conda_package` script determines the type of a
project by inspecting the `meta.yaml` file in the conda recipe.


To clarify, the CI conda build jobs are configured so that:

1. Platform independent recipes will be built as `noarch` packages.
2. C/C++ recipes will be built as `linux-64`, `linux-aarch64`, `macos-64`, and
   `macos-M1` packages.
3. CUDA recipes will be built as `linux-64-cuda-X.Y` packages - a separate
   package is built for each supported CUDA version.


> *Note:* The `FSLCONDA_SKIP_PLATFORM` variable can be set to prevent jobs
> for certain platforms from being created - setting this variable is
> advised, to reduce unnecessary creation of jobs, and thus to reduce costs.
> The `configure_repositories` command will set this variable on recipe
> repositories automatically.


### FSL CUDA projects


Most FSL CUDA projects usually provide both GPU-enabled and CPU-only
executables. For example, the [`fsl/fdt`](https://git.fmrib.ox.ac.uk/fsl/fdt)
provides a range of CPU-only executables, including `dtifit` and `vecreg`, in
addition to providing GPU-enabled executables such as `xfibres_gpu`.


To accommodate this convention, multiple conda recipes are used for these
"hybrid" projects. For example, the `fsl/fdt` project is built from two
separate recipes:


 - [`fsl/conda/fsl-fdt`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt), which
   builds the CPU-only executables - these recipes are built as `linux-64`,
   `linux-aarch64`, `macos-64`, and `macos-M1` packages.
 - [`fsl/conda/fsl-fdt-cuda`](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-fdt-cuda),
   which builds the CUDA/GPU-enabled executables - these recipes are built as
   `linux-64-cuda-X.Y` packages. A separate package is built for each supported
   CUDA version.


CUDA packages may be compiled against a range of CUDA Toolkit versions; the
resulting conda package will be labelled with the CUDA version it was compiled
against, as outlined above. The `FSLCONDA_CUDA_VERSION` variable can be used to
control which CUDA versions a package is built for - this variable must be set
on the project recipe repository, and must contain a space-separated list of
all CUDA versions that should be built, e.g. `"9.2 10.0 11.3"`.

The standard convention is for CUDA projects to be compiled in such a way as
to ensure maximum possible compiatibilty with different GPU hardware:

 - The CUDA Toolkit libraries are statically linked into the executables.
 - CUDA code is built against the widest possible range of GPU architectures,
   with forward-compatibility for newer architectures.

So a CUDA package compiled against, for example, CUDA 9.2, should work with
any GPU driver/hardware which is compatible with CUDA 9.2 or newer.

These are conventions only - it is the responsibility of the project
`Makefile` (e.g. fsl/eddy>) and package recipe `build.sh` scripts
(e.g. fsl/conda/fsl-eddy-cuda>) to adhere to them.  Compilation rules for CUDA
projects are implemented in the fsl/base> project.


### Disable builds for specific platforms


A situation may arise whereby the above behaviour is not suitable. For
example, a particular project may only be supported on `linux-64`, or may only
support specific versions of CUDA. The `FSLCONDA_SKIP_PLATORM` variable can be
used to disable building of conda packages for specific platforms - it must be
a space separated list containing the platforms to be skipped, where valid
platform names are:


 - `linux-64`
 - `linux-aarch64`
 - `macos-64`
 - `macos-M1`
 - `noarch`
 - `linux-64-cuda` (skips all CUDA builds)
 - `linux-64-cuda-9.2` (skips the CUDA 9.2 build)


### Maintenance releases


Stable packages for a given project are typically built and published from a
branch on the recipe repository named `master` or `main`. This branch should
contain a build recipe for the latest available version of the project.
However it is possible to publish maintenance releases for an older version of
the project, if the need arises. This can be done by creating a branch named
`maint/<release-series>`, where `<release-series>` contains the version string
prefix of the maintenance release series.

For example, imagine the following scenario:

 - Versions `3.1.0` and `3.2.0` of a package have already been published.
 - The `master` branch of the recipe repository is configured to build version
   `3.2.0`.
 - A bug is found, but an important user is unable to upgrade to version
   `3.2.0` for some reason. So the bug fix needs to be applied to both the
   `3.1` and `3.2` series, and new versions `3.1.1` and `3.2.1` need to be
   released.

This can be achieved like so:
 1. Create a new branch on the recipe repository called `maint/3.1`, and
    adjust the recipe on that branch to build version `3.1.1`.
 2. Update the `master` branch on the recipe repository to build version
    `3.2.1`.
 3. Publish the built packages from both branches.

**Note** that packages cannot be published from branches with any name other
than `main`, `master`, or `maint/<release-series>`


## Configuration


In order to interact with Gitlab projects, a Gitlab API access token, which
has full (read+write) access to the Gitlab API, must be created. Access tokens
can be created via the Gitlab web interface. This token will be stored in a
variable on all repositories - see the steps below.


> **Note:** The remaining steps can be performed automatically with the
> `configure_repositories` command mentioned above.  Alternately, they can be
> performed via the Gitlab web interface.


On **both project and recipe repositories** you must enable at least one
Gitlab CI runner:

 - One runner which is installed on the server that hosts the conda channel
   directories, tagged with `fslconda-channel-host`. This runner will be used
   to run CI jobs that deploy built conda packages to the FSL conda
   channels. More details on how this runner needs to be configured can be
   found below.

 - One or more runners with the `fsl-ci` and `docker` tags. These runners will
   be used to execute any CI jobs that do not involve deploying conda
   packages, including building `noarch` packages, and binary packages for
   x86-64 Linux.

 - One or more runners with the `fsl-ci`, `docker`, and `linux-aarch64`
   tags. These runners will be used to build and test binary packages for
   ARM64 linux.

 - One or more runners with the `fsl-ci` and `macOS-64` tags. These runners
   will be used to build and test binary packages for x86-64 macOS. An
   assumption is made that `conda` is installed and available in the
   environment provided by these runners.

 - One or more runners with the `fsl-ci` and `macOS-M1` tags. These runners
   will be used to build and test binary packages for ARM64 macOS. An
   assumption is made that `conda` is installed and available in the
   environment provided by these runners.

Next, on **both project and recipe repositories**, for a FSL project which does
not require any custom CI rules of its own (most FSL projects fall into this
category), all that needs to be done is to set the *Custom CI configuration
path* option to refer to this file, by giving it the value:

    .gitlab-ci.yml@fsl/conda/fsl-ci-rules

This option can be found under _Settings -> CI / CD -> General Pipelines_,
in the Gitlab web interface for each project.

Finally, you need to set the following variables in the _Settings -> CI / CD ->
Variables_ section of the Gitlab web interface. More detail on these variables
can be found in the **Configuration** section, below:

 - `FSL_CI_API_TOKEN`: Required on both **project** and **recipe**
   repositories. Gitlab API token which is used by CI jobs for read+write
   access via the Gitlab REST API.

 - `FSLCONDA_RECIPE`: Required on the **recipe** repository. A flag which is
   used to distinguish project repositories from recipe repsitories. The value
   is not checked - only whether or not the variable is set.

 - `FSLCONDA_RECIPE_URL`: Required on the **project** repository, but only
   when its accompanying recipe repository is not named according to the
   `fsl/conda/fsl-<project>` convention. Contains the full URL to the conda
   recipe repository.

 - `FSLCONDA_BUILD_PLATFORM`: Required on the **recipe** repository
   to determine which platforms a package should be built for  (e.g. `noarch`
   `linux-64`, `linux-aarch64` `macos-64`, `macos-M1` and/or
   `linux-64-cuda-10.2`).

 - `FSLCONDA_INTERNAL`: Optional. Can be set on the **recipe** repository. If
   set, built conda packages are deployed to the _internal_ conda channel,
   rather than the _public_ conda channel.

The above instructions should suffice for most FSL projects. However, for a FSL
project which already has its own `.gitlab-ci.yml` configuration file, instead
of setting the *Custom CI configuration path* setting on the **project
repository**:

 1. The `.gitlab-ci.yml` file of the project should include the `.gitlab-ci.yml`
    file of this repository via the following section - this should be at the
    **beginning** of `.gitlab-ci.yml`:

        include:
         - project: fsl/conda/fsl-ci-rules
           file:    .gitlab-ci.yml

 2. `fsl-ci-pre` `fsl-ci-build`, `fsl-ci-test` and `fsl-ci-deploy` should be
    added to the CI stages, in that order. For example:

        stages:
          - build
          - test
          - fsl-ci-pre
          - fsl-ci-build
          - fsl-ci-test
          - fsl-ci-deploy


### Private/internal packages


By default, all built conda packages are deployed to the _public_ FSL conda
channel, and made publicly available to the world. Certain packages may
be intended for private/internal use only, and can instead be deployed to
an _internal_ conda channel, to which access is restricted by requiring
login credentials.

To configure a package to be deployed to the _internal_ channel, the
following CI / CD variables must be set  on the **recipe** repository:

 - `FSLCONDA_INTERNAL`: Marks this recipe as an internal package
 - `FSLCONDA_INTERNAL_CHANNEL_USERNAME`: Username for accessing the internal
    conda channel
 - `FSLCONDA_INTERNAL_CHANNEL_PASSWORD`: Password for accessing the internal
    conda channel

Packages which are deployed to the _public_ FSL conda channel **must not**
depend on packages deployed to the _internal_ channel.


### Development packages


A situation may arise whereby a developer wishes to build and publish an
unstable / development / pre-release version of a FSL project. This is
possible by running the `trigger-devrelease-package-build` CI job on any
commit in the project repository. This will cause a conda package to be built
against that specific commit, given a version of
`<last-stable-tag>.<date>.dev0+<commit>`, and published to the _development_
conda channel. See the `development_workflow` page at fsl/conda/docs> for more
information.


### Disabling rules on project repositories


If you are maintaining a project repository in which the `fsl-ci-rules` have
been integrated, and would like to disable automatic package building and
recipe updates, you can disable them by setting a CI variable called
`FSL_CI_SKIP_ALL`, and giving it a non-empty value. This may be desirable if
you have your own personal fork of a FSL project repository, and do not wish
to run the standard FSL CI rules. See the **Summary of environment variables**
section below for more fine-grained customisation options.


### Configuring the channel host Gitlab runner


In order to facilitate automatic deployment of built conda packages:


 1. The Gitlab CI runner which is used to run the deployment jobs must be
    running on a server which has access to the conda channel directories.

 2. The runner must have the `fslconda-channel-host` tag.

 3. In `.gitlab-ci.yml` of *this* repository, the following variables must be
    set, denoting the URLs, and locally accessible directories, of the conda
    channels:
      - `FSLCONDA_PUBLIC_CHANNEL_URL`: `https://` URL of the public channel
      - `FSLCONDA_DEVELOPMENT_CHANNEL_URL`: `https://` URL of the development
        channel
      - `FSLCONDA_INTERNAL_CHANNEL_URL`: `https://` URL of the internal
        channel
      - `FSLCONDA_PUBLIC_CHANNEL_DIRECTORY`: Locally accessible directory
        containing the public channel
      - `FSLCONDA_DEVELOPMENT_CHANNEL_DIRECTORY`: Locally accessible directory
        containing the development channel
      - `FSLCONDA_INTERNAL_CHANNEL_DIRECTORY`: Locally accessible directory
        containing the internal channel


## Docker images


For macOS jobs, manually managed physical/virtual macOS machines are used.
For Linux, and for platform-independent jobs, custom Docker images are used.
The images which are used for different Linux archiectures are defined in
`.gitlab-ci.yml`, via the `FSL_CI_IMAGE_*` variables. Each custom image is
defined by a `Dockerfile` within the `docker` sub-directory of this
repository.


These images are currently hosted on Docker Hub, at
https://hub.docker.com/orgs/fsldevelopment/, although they may be hosted
elsewhere in the future. Image tags are of the form:

  `fsldevelopment/fsl-<platform>:<YYYYMMDD>.<commit>`

where
 - `<platform>` is the platform label (one of `linux-64`, ...).
 - `<YYYYMMDD>` is the date that the image was built and published
 - `<commit>` is the short SHA hash of the commit on the `fsl-ci-rules`
    git repository (this repository) from which the image was built.


## CI management jobs


A number of miscellaneous CI jobs are configured for management purposes;
these can be invoked manually on an as-needed basis.  These rules are
defined in `rules/fsl-ci-management-rules.yml`, and can be manually
triggered via the Gitlab UI for the `fsl-ci-rules` repository.

 - `build-docker-image-*`: CI rules are defined to re-build each Docker image,
   and upload them to the Docker Hub.

 - `purge-channel-indexes`: This CI job deletes and re-generates the indexes
   for both the public and internal channels.

 - `clear-public-channel`: This CI job can be used to delete packages from the
   public channel.

 - `clear-internal-channel`: This CI job can be used to delete packages from
   the internal channel.


## Summary of environment variables


This table contains a summary of all environment variables which can be used
to control / customise the behaviour of the FSL CI rules.


| Name                                  | Repository | Required | Purpose                                                                                                                                                                                             |
| ------------------------------------- | ---------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `FSL_CI_RULES_REPOSITORY`             | Both       | No       | Install CI rules from a different repository.                                                                                                                                                       |
| `FSL_CI_RULES_REVISION`               | Both       | No       | Install CI rules from a branch other than `master`.                                                                                                                                                 |
| `FSL_CI_API_TOKEN`                    | Both       | Yes      | GitLab API token used for interacting with GitLab.                                                                                                                                                  |
| `FSL_CI_SKIP_ALL`                     | Both       | No       | Skip all FSL CI jobs.                                                                                                                                                                               |
| `FSL_CI_SKIP_TEST`                    | Recipe     | No       | Skip automatic `pyfeeds` unit tests.                                                                                                                                                                |
| `FSL_CI_TEST_REQUIRES`                | Recipe     | No       | Space-separated list of conda packages install when running tests (in addition to those required by the package).                                                                                   |
| `FSL_CI_GIT_NEED_AUTHENTICATION`      | Recipe     | No       | Set this if the project git repository is not publicly accesssible.                                                                                                                                 |
| `FSLCONDA_RECIPE`                     | Recipe     | Yes      | Used to differentiate recipe repositories from project repositories.                                                                                                                                |
| `FSLCONDA_REPOSITORY`                 | Recipe     | No       | Build a conda package from this repository, instead of the repository specified in `meta.yaml`.                                                                                                     |
| `FSLCONDA_REVISION`                   | Recipe     | No       | Build a conda package from this revision, instead of the revision specified in `meta.yaml`.                                                                                                         |
| `FSLCONDA_RECIPE_URL`                 | Project    | No       | Space separated list of recipe repository URLs, where the `fsl/<project>` / `fsl/conda/fsl-<project>` naming convention is not followed, or where multiple recipes are associated with one project. |
| `FSLCONDA_BUILD_EXTRA_ARGS`           | Recipe     | No       | (_Advanced_) Additional arguments to pass to `conda build`.                                                                                                                                         |
| `FSLCONDA_USE_BOA`                    | Recipe     | No       | (_Advanced_) Use `boa` instead of `conda build` for improved performance.                                                                                                                           |
| `FSLCONDA_BUILD_CONFIG_TEMPLATE`      | Recipe     | No       | (_Advanced_) Jinja2 `conda_build_config.yaml` template, overriding `fsl_ci/templates/conda_build_config.yaml`.                                                                                      |
| `FSLCONDA_BUILD_PLATFORM`             | Recipe     | Yes      | Space-separated list of conda package platforms which should be built - see below.                                                                                                                  |
| `FSLCONDA_SKIP_PLATFORM`              | Recipe     | No       | Space-separated list of conda package platforms which should not be built. Superseded by, but takes precedence over, `FSLCONDA_BUILD_PLATFORM`.                                                     |
| `FSLCONDA_CUDA_VERSION`               | Recipe     | No       | Space-separated list of CUDA versions (e.g. `"9.2 10.2"`) denoting which CUDA versions a project should be built against.                                                                           |
| `FSLCONDA_SKIP_RECIPE_UPDATE`         | Project    | No       | Do not open a merge request on the recipe repository when new tags are added to the project repository.                                                                                             |
| `FSLCONDA_SKIP_MANIFEST_UPDATE`       | Project    | No       | Do not open a merge request on the fsl/conda/manifest> repository when new tags are added to the project repository.                                                                                |
| `FSLCONDA_DEPENDANT_RECIPES`          | Recipe     | No       | Space-separated list of recipe paths (e.g. `fsl/conda/fsl-avwutils`) which should be updated by the `update-dependant-recipes` CI job                                                               |
| `FSLCONDA_INTERNAL`                   | Recipe     | No       | Marks this conda package as internal - builds will be deployed to the _internal_ channel, rather than the _public_ channel.                                                                         |
| `FSLCONDA_INTERNAL_CHANNEL_USERNAME`  | Recipe     | No       | Username for accessing the internal conda channel - required for packages which depend on internal-only packages .                                                                                  |
| `FSLCONDA_INTERNAL_CHANNEL_PASSWORD`  | Recipe     | No       | Password for accessing the internal conda channel - required for packages which depend on internal-only packages .                                                                                  |
| `FSL_MANIFEST_REPOSITORY`             | Recipe     | No       | Download `fsl-release.yml` from this repository when building a package (versions of base/core packages are specified in this file, and pinned at build time).                                      |
| `FSL_MANIFEST_REVISION`               | Recipe     | No       | Download `fsl-release.yml` from this branch when building a package.                                                                                                                                |


## Adding support for a new CUDA version


Just follow these simple steps:


1.  Create a `Dockerfile` for the new CUDA version in the `docker`
    sub-directory.
2.  Add a `FSL_CI_IMAGE_LINUX_64_CUDA_X_Y` variable in
    the `.gitlab-ci.yml` file.
3.  Add a  `build-docker-image-linux-64-cuda-X.Y` job to the
    `rules/fsl-ci-management-rules.yml` file.
4.  Add a `build-linux-64-cuda-X.Y-conda-package` job to the
    `rules/fsl-ci-build-rules.yml` file.
5.  Add a `deploy-linux-64-cuda-X.Y-conda-package` job to the
    `rules/fsl-ci-deploy-rules.yml` file.
6.  Update the `$FSLCONDA_SKIP_PLATFORM` clause in the
    `.fsl-ci-conda-build-job-base` job template in the
    `rules/fsl-ci-build-rules.yml` file.
7.  Update the `$FSLCONDA_SKIP_PLATFORM` clause in the `.deploy-conda-package`
    job template in the `rules/fsl-ci-deploy-rules.yml` file.
8.  Update the `CUDA_VERSIONS` list in `fsl_ci/platform.py`.
9.  Ensure that an appropriate version of GCC is being installed in
    the `docker/install_miniconda.sh` file.
10. Ensure that the CUDA conda recipe template specifes an appropriate
    version of GCC in the `fsl_ci/templates/meta.yaml.cuda.template` file,
    **and also in the recipes for all existing CUDA projects**.
11. Update the `FSLCONDA_CUDA_VERSION` variable on all CUDA recipe repositories
    as needed.
