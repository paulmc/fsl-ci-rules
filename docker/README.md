# Docker images for `fsl-ci-rules`

This directory contains Dockerfiles which are used by the fsl-ci-rules for building FSL projects on different platforms.


These images are currently hosted at https://hub.docker.com/orgs/fsldevelopment/


Each `Dockerfile` is written so as to be built relative to the `docker` directory, e.g., from the root directory of `fsl-ci-rules`:


```
docker build -f docker/linux-64/Dockerfile docker
```


These images can be re-built and re-deployed by manually running the `build-docker-image-*` jobs on the `fsl/fsl-ci-rules` gitlab repository. These jobs are defined in the `rules/fsl-ci-management-rules.yml` file.


The full name of each image is automatically generated from the directory name, by prefixing it with `fsldevelopment/fsl-`. For example, the image built from the `Dockerfile` within the `linux-64` will be named `fsldevelopment/fsl-linux-64`.


Whenever a new version of an image `<image>` is built, it is tagged with `<image>:latest`, and with `<image>:YYYYMMDD.<commitsha>`, where `YYYYMMDD` is the current date, and `<commitsha>` is the short SHA checksum of the `fsl/fsl-ci-rules` commit associated with the build.


## Docker build image

The `docker-build-qemu` image is used for building multi-platform images, which in turn are used for building Linux conda packages.


## CUDA images

Prior to August 2023, the CUDA images were based on [official docker images maintained by NVIDIA](https://hub.docker.com/r/nvidia/cuda). Unfortunately in July 2023 NVIDIA adopted the policy of [deleting old images](https://gitlab.com/nvidia/container-images/cuda/-/issues/209), which made it impossible to update our `linux-64-cuda-10.2` image.


So now our `linux-64-cuda-10.2` image is based on our own version of the `nvidia/cuda:10.2-devel-centos7` image, hosted at `fsldevelopment/nvidia-cuda:10.2-devel-centos7`.

This image is built from a copy of the https://gitlab.com/nvidia/container-images/cuda repository, hosted at https://git.fmrib.ox.ac.uk/paulmc/cuda-docker-images. This copy was created like so:

```
git clone https://gitlab.com/nvidia/container-images/cuda
cd cuda

# Check out the state of the master branch as of 25th August 2023
git checkout 81682547e12c8807ebc5fa61ff4576510925a324

# Re-instate the 10.2 image
mv dist/end-of-life/10.2 dist/
```

The image can then be built as follows:

```
git clone https://git.fmrib.ox.ac.uk/paulmc/cuda-docker-images.git
cd cuda
./build.sh -d --image-name fsldevelopment/nvidia-cuda \
              --cuda-version 10.2                     \
              --os centos                             \
              --os-version 7                          \
              --arch x86_64                           \
              --push
```
