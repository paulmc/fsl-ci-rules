#!/usr/bin/env bash

set -e

CONDABIN=micromamba

# Download+install micromamba
export MAMBA_ROOT_PREFIX=/micromamba
export PATH=$MAMBA_ROOT_PREFIX/bin:$PATH
mkdir -p $MAMBA_ROOT_PREFIX/bin
cd $MAMBA_ROOT_PREFIX

if   [[ $(uname -m) == "x86_64" ]]; then
  url="https://micro.mamba.pm/api/micromamba/linux-64/latest"
elif [[ $(uname -m) == "aarch64" ]]; then
  url="https://micro.mamba.pm/api/micromamba/linux-aarch64/latest"
fi

curl -Ls ${url} | tar -xvj bin/micromamba
cd /

# Update conda and install conda-build
# and fsl-ci-rules deps basics into the
# base env. Only specify boa and not
# mamba/conda/conda-build, as boa has
# strict pinning requirements w.r.t
# conda/conda-build/mamba.
micromamba install -n base -y -c conda-forge \
  python=3.11 boa \
  patch git git-lfs tini \
  jinja2 \
  numpy \
  networkx \
  ruamel.yaml \
  ruamel.yaml.jinja2

# initialise git lfs
git lfs install

# Different versions of CUDA require
# different versions of GCC
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html
if   [ "$CUDA_VER" = "9.2"  ] || \
     [ "$CUDA_VER" = "10.0" ]; then GCC='7.*'
elif [ "$CUDA_VER" = "10.1" ] || \
     [ "$CUDA_VER" = "10.2" ]; then GCC='8.*'
elif [ "$CUDA_VER" = "11.0" ] || \
     [ "$CUDA_VER" = "11.1" ] || \
     [ "$CUDA_VER" = "11.2" ] || \
     [ "$CUDA_VER" = "11.3" ]; then GCC='9.*'
elif [ "$CUDA_VER" = "11.4" ] || \
     [ "$CUDA_VER" = "11.5" ] || \
     [ "$CUDA_VER" = "11.6" ]; then GCC='11.*'
else                                GCC='*'
fi

# Cache some packages to speed up CI
# (https://github.com/conda-forge/docker-images/blob/master/linux-anvil-cuda/Dockerfile)
${CONDABIN} create -n test --yes --quiet --download-only \
    conda-forge::boost-cpp \
    conda-forge::qt-main \
    conda-forge::scipy \
    conda-forge::icu \
    conda-forge::libclang \
    conda-forge::libllvm15 \
    conda-forge::ffmpeg \
    conda-forge::sysroot_linux-64 \
    conda-forge::binutils_impl_linux-64 \
    conda-forge::binutils_linux-64 \
    conda-forge::gcc_impl_linux-64 \
    conda-forge::gcc_linux-64="$GCC" \
    conda-forge::gfortran_impl_linux-64 \
    conda-forge::gfortran_linux-64 \
    conda-forge::gxx_impl_linux-64 \
    conda-forge::gxx_linux-64="$GCC" \
    conda-forge::libgcc-ng \
    conda-forge::libgfortran-ng \
    conda-forge::libstdcxx-ng
${CONDABIN} remove --yes --quiet -n test --all
${CONDABIN} clean -tiy
