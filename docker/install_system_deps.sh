#!/usr/bin/env bash

set -e

# Install:
#  - basics
#  - tools to mount nfs4 shares
#  - mesa-libGL, as pyopengl cannot be
#    installed unless libGL is present
yum install -y wget \
               bc \
               bzip2 \
               tar \
               curl \
               which \
               util-linux-ng \
               nfs-utils \
               mesa-libGL
